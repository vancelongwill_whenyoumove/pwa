import { DeepPartial } from "ts-essentials"
import { compose } from "redux"

import { IState } from "../shared/store/types"

declare global {
  // eslint-disable-next-line @typescript-eslint/interface-name-prefix
  interface Window {
    __PRELOADED_STATE__?: DeepPartial<IState>
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose
  }
}
