import * as React from "react"
import * as ReactDOM from "react-dom"
import { BrowserRouter } from "react-router-dom"

import { Provider } from "react-redux"
// import { PersistGate } from "redux-persist/integration/react"

import { makeStore } from "../shared/store"

import Root from "../shared/components/Root"

// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__

// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__

const { store, persistor } = makeStore(preloadedState)

const App: React.FunctionComponent<{ RootComponent: typeof Root }> = ({
  RootComponent
}) => (
  <Provider store={store}>
    <BrowserRouter>
      <RootComponent />
    </BrowserRouter>
  </Provider>
)

// Use this logic instead of the PersistGate component to avoid mismatch between server and client HTML
// NB: this means that the UI will not wait until the persisted state is retrieved before rendering
persistor.subscribe(() => {
  // Hydrate React components when persistor has synced with redux store
  const { bootstrapped } = persistor.getState()

  if (bootstrapped) {
    ReactDOM.hydrate(
      <App RootComponent={Root} />,
      document.getElementById("root")
    )
  }
})

// HMR
if (module.hot) {
  module.hot.accept("../shared/components/Root", () => {
    const NextRoot = require("../shared/components/Root").default
    ReactDOM.hydrate(
      <App RootComponent={NextRoot} />,
      document.getElementById("root")
    )
  })
}
