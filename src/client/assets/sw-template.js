workbox.core.clientsClaim()
workbox.core.skipWaiting()

self.__precacheManifest = [].concat(self.__precacheManifest || [])

// precahce and route asserts built by webpack
workbox.precaching.precacheAndRoute(self.__precacheManifest, {})

// return app shell for all navigation requests
workbox.routing.registerNavigationRoute(
  workbox.precaching.getCacheKeyForURL("/app-shell")
)

// routing for api
workbox.routing.registerRoute(
  // offline use for the dog api
  /^https:\/\/api.thedogapi.com\/v1\/images\/search\?q=\w/i,
  new workbox.strategies.NetworkFirst({
    cacheName: "WYM-api-cache"
  })
)

// routing for cloud served images
workbox.routing.registerRoute(
  /^https:\/\/.+\.(jpe?g|png|gif|svg)$/i,
  new workbox.strategies.CacheFirst({
    cacheName: "WYM-cloud-image-cache",
    plugins: [
      new workbox.expiration.Plugin({
        // Only cache requests for a week
        maxAgeSeconds: 7 * 24 * 60 * 60,
        // Only cache 20 requests.
        maxEntries: 20
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      })
    ]
  })
)

// google maps streetview images & google maps static map images
workbox.routing.registerRoute(
  /^https:\/\/maps.googleapis.com\/maps\/api\/(streetview|staticmap)\?.*/,
  new workbox.strategies.CacheFirst({
    cacheName: "WYM-streetview-staticmap-image-cache",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      })
    ]
  })
)
