// tslint:disable object-literal-sort-keys
import { DeepPartial } from "ts-essentials"

import panoramaData from "../__stubs__/googleGetPanoramaByLocationData"

export default function(): DeepPartial<typeof google> {
  class StreetViewService {
    public getPanoramaByLocation = jest
      .fn()
      .mockImplementation((_0, _1, cb) => {
        cb(panoramaData, "OK")
      })
  }

  const computeHeading = jest.fn().mockImplementation(() => 20)

  const maps: DeepPartial<typeof google.maps> = {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    StreetViewStatus: { OK: "OK" as any },
    StreetViewService,
    geometry: {
      spherical: {
        computeHeading
      }
    }
  }

  const googleMock: DeepPartial<typeof google> = { maps }

  return googleMock
}
