import { DeepPartial } from "ts-essentials"

export default function(): DeepPartial<Navigator> {
  const getUserMedia = jest
    .fn<Promise<MediaStream>, [MediaStreamConstraints]>()
    .mockImplementation(() => {
      return Promise.resolve(new Object() as MediaStream)
    })

  const navigator: DeepPartial<Navigator> = {
    mediaDevices: { getUserMedia }
  }

  return navigator
}
