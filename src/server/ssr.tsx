import * as express from "express"
import * as React from "react"

import { renderToString } from "react-dom/server"
import { IRenderer } from "./render"

import { Provider } from "react-redux"
import { StaticRouter } from "react-router-dom"

import { removePersistedKeys } from "./utils"

import Root from "../shared/components/Root"
import { makeStore } from "../shared/store"
import { IState } from "../shared/store/types"

import { DeepPartial } from "ts-essentials"

const appshellRegex = /\/app-shell.*/g

const ssr = (): express.RequestHandler => (req, res, next) => {
  const render: IRenderer["render"] = req.app.locals.render
  if (typeof render !== "function") {
    throw new Error(`
      SSR requires a render to html function at app.locals.render with the following signature:
        render(renderedApp: string, preloadedState: any): string
    `)
  }
  // NB: preloaded state derived from the `req` object must be sanitized to protect again XSS attacks
  const preloadedState: DeepPartial<IState> = {
    dogs: {
      dogs: [
        {
          id: "asdasd823",
          url: "https://source.unsplash.com/random/1200x900"
        },
        {
          id: "as7a7d823",
          url: "https://source.unsplash.com/random/500x500"
        },
        {
          id: "as7a7e823",
          url: "https://source.unsplash.com/random/400x300"
        },
        {
          id: "as7a9d823",
          url: "https://source.unsplash.com/random/500x400"
        },
        {
          id: "as7a7d829",
          url: "https://some.unreachable.url.com/random/800x400"
        }
      ]
    }
  }

  const { store } = makeStore(preloadedState)
  // React router context, this can be used to check for redirects
  const context: { url?: string } = {}

  // run sagas & wait for sagas tasks to finish before the final render
  store
    .runSaga()
    .toPromise()
    .then(() => {
      // Redirects (for now) are client-side, to allow use of client side persisted auth tokens
      // The below code would enable redirects on the server side
      // if (context.url) {
      //   return res.redirect(301, context.url)
      // }

      // get redux store's state after work has been performed
      const finalState: IState = store.getState()
      // Redux-persist-ed keys cannot be hydrated properly if they are supplied already in initialState
      // Don't send preloaded state for the `/app-shell` route
      const filteredFinalState = appshellRegex.test(req.url)
        ? {}
        : removePersistedKeys(finalState, ["properties"])

      // do a final render of the app with the new state
      const renderedApp = renderToString(
        <Provider store={store}>
          <StaticRouter location={req.url} context={context}>
            <Root />
          </StaticRouter>
        </Provider>
      )

      res.send(render(renderedApp, filteredFinalState))
      res.end()
      next()
    })

  // do an initial render to trigger sagas
  renderToString(
    <Provider store={store}>
      <StaticRouter location={req.url} context={context}>
        <Root />
      </StaticRouter>
    </Provider>
  )

  // close sagas which are still watching for actions (this does not effect sagas already performing tasks)
  store.closeSaga()
}

export default ssr
