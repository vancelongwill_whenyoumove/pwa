import * as cheerio from "cheerio"
import * as express from "express"

import { readFileSync } from "fs"

import { DeepPartial } from "ts-essentials"
import { IState } from "../shared/store/types"

export interface IRenderer {
  render(renderedApp: string, preloadedState: DeepPartial<IState>): string
}

function buildPreloadedStateHtml(preloadedState: DeepPartial<IState>): string {
  return `<script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // http://redux.js.org/recipes/ServerRendering.html#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(
            /</g,
            "\\u003c"
          )}
      </script>`
}

export function render(
  bundledHtmlString: string,
  additionalScripts: string = ""
): IRenderer["render"] {
  // return a function which attaches the server rendered app and returns the html as a string
  return (renderedApp: string, preloadedState: DeepPartial<IState>) => {
    const $ = cheerio.load(bundledHtmlString)
    // insert the server rendered html string
    $("div#root").html(renderedApp)
    // append the preloaded redux state
    $("body").after(buildPreloadedStateHtml(preloadedState))
    // append any additional scripts we might want to render dynamically
    // other normal scripts should be placed in the `src/shared/assets/index.html` template
    if (additionalScripts) {
      $("body").after(additionalScripts)
    }
    return $.html()
  }
}

export function readHtmlFile(htmlFilePath: string): string {
  // read & parse the webpack generated html output
  let bundledHtmlString: string
  try {
    bundledHtmlString = readFileSync(htmlFilePath, "utf8")
  } catch (e) {
    // tslint:disable-next-line no-console
    console.error("Error reading bundled HTML from file: ", e.message)
    throw e
  }

  return bundledHtmlString
}

interface ICreateRenderMiddlewareOptions {
  htmlFilePath: string
  isProduction?: boolean
}
// middleware exposing our render function
function createRenderMiddleware({
  htmlFilePath,
  isProduction = true
}: ICreateRenderMiddlewareOptions): express.RequestHandler {
  return (req, _, next) => {
    // only build the renderer once to avoid reading from file for every request
    if (isProduction && req.app.locals.render) {
      next()
      return
    }
    const bundledHtmlString = readHtmlFile(htmlFilePath)
    req.app.locals.render = render(bundledHtmlString)
    next()
  }
}

export default createRenderMiddleware
