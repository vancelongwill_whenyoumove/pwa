import { Stats } from "webpack"

import { DeepPartial } from "ts-essentials"
import { IState } from "../shared/store/types"

// Recursively checks for a nested "_persist" key, returns true as soon as one is found
function hasPersistKey<T>(stateSlice: T): boolean {
  return Object.entries(stateSlice).some(([key, value]) => {
    if (key === "_persist") {
      return true
    }
    if (typeof value === "object") {
      return hasPersistKey(value)
    }
    return false
  })
}

// Removes top level state keys which contain a nested "_persist" key, indicating that they will instead be hydrated on the client side
export function removePersistedKeys(
  state: DeepPartial<IState>,
  whitelist?: string[]
): DeepPartial<IState> {
  return Object.entries(state).reduce((acc, [key, value]) => {
    const isWhitelisted = whitelist && whitelist.includes(key)
    if (!isWhitelisted && hasPersistKey(value)) {
      return acc
    }
    return {
      ...acc,
      [key]: value
    }
  }, {})
}

// Prints more detailed webpack error messages
export function webpackErrorHandler(
  err: Error & { details?: string },
  stats: Stats
): void {
  // tslint:disable no-console
  if (err) {
    console.error(err.stack || err)
    if (err.details) {
      console.error(err.details)
    }
    return
  }

  const info = stats.toJson()

  if (stats.hasErrors()) {
    console.error(info.errors)
  }

  if (stats.hasWarnings()) {
    console.warn(info.warnings)
  }
  // tslint:enable no-console
}
