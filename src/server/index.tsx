// tslint:disable no-console
import * as express from "express"

import * as webpack from "webpack"
import * as webpackDevMiddleware from "webpack-dev-middleware"
import * as webpackHotMiddleware from "webpack-hot-middleware"

// import * as webpackHotServerMiddleware from "webpack-hot-server-middleware"

import webpackConfig from "../../webpack.development.config"
import ssrWebpackConfig from "../../webpack.development.ssr.config"

import { DEBUG_WEBPACK, NODE_ENV, PORT } from "../shared/config/env"

import { webpackErrorHandler } from "./utils"

import createRenderHtmlMiddleware, { render } from "./render"
import ssr from "./ssr"

const port = PORT || 3000
const isProduction = NODE_ENV === "production"
const server = express()

if (!isProduction) {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const webpackHotServerMiddleware = require("webpack-hot-server-middleware")

  // tslint:disable-next-line no-console
  console.log("Running in development mode with HMR enabled\n")

  const config: webpack.Configuration[] = [
    ssrWebpackConfig, // compile with src/server/ssr.tsx as the entry point
    webpackConfig[1] // compile client bundle as normal
  ]
  // build with webpack
  let compiler: webpack.MultiCompiler
  if (DEBUG_WEBPACK) {
    // attaches a stat/error logger to the webpack compiler
    compiler = webpack(config, webpackErrorHandler) as webpack.MultiCompiler
  } else {
    compiler = webpack(config)
  }

  if (!webpackConfig[1].output) {
    throw new Error(
      `Something went wrong building webpack for ${webpackConfig[1].name}`
    )
  }

  const devMiddleware = webpackDevMiddleware(compiler, {
    publicPath: webpackConfig[1].output.publicPath || "",
    serverSideRender: true
  })

  // attach HMR related middlewares
  server.use(devMiddleware)

  const clientCompiler = compiler.compilers.find(c => c.name === "client")

  if (!clientCompiler) {
    throw new Error(`Expected a webpack config with name 'client'`)
  }

  server.use(
    webpackHotMiddleware(clientCompiler, {
      path: "/__webpack_hmr"
    })
  )
  // get the webpack-generated html file from the in memory fs and attach a html render function
  // attaches a render function to server.locals.render which we later use in the ssr
  interface IOutputFileSystemWithReadFile extends webpack.OutputFileSystem {
    readFile: (fn: string, cb: (e: Error, res: string) => void) => void
  }
  server.use((_0, _1, next) => {
    devMiddleware.waitUntilValid(() => {
      const filename = `${clientCompiler.outputPath}/public/dev-index.html`
      ;(clientCompiler.outputFileSystem as IOutputFileSystemWithReadFile).readFile(
        filename,
        (err, result) => {
          if (err) {
            throw err
          }

          server.locals.render = render(result)
          next()
        }
      )
    })
  })

  // HMR for `src/server/ssr.tsx` meaning all changed to our react app are hot reloaded
  // this middleware also auto attaches the ssr middleware
  server.use(
    webpackHotServerMiddleware(compiler, {
      chunkName: "ssr"
    })
  )
} else {
  // attaches a render function to server.locals.render which we later use in the ssr
  server.use(
    createRenderHtmlMiddleware({ htmlFilePath: "./public/index.html" })
  )
  // Serve static assets from the public folder
  // { index: false } prevents our generated index.html from being sent
  server.use(express.static("public", { index: false }))

  // use the ssr middleware for all incoming get requests
  server.get("/*", ssr())
}

server.listen(port, () => console.log(`WYM app listening on port ${port}!`))
