/* eslint-disable @typescript-eslint/camelcase */
const place: google.maps.places.PlaceResult = {
  address_components: [
    { long_name: "25", short_name: "25", types: ["street_number"] },
    {
      long_name: "Christopher Street",
      short_name: "Christopher St",
      types: ["route"]
    },
    { long_name: "London", short_name: "London", types: ["postal_town"] },
    {
      long_name: "Greater London",
      short_name: "Greater London",
      types: ["administrative_area_level_2", "political"]
    },
    {
      long_name: "England",
      short_name: "England",
      types: ["administrative_area_level_1", "political"]
    },
    {
      long_name: "United Kingdom",
      short_name: "GB",
      types: ["country", "political"]
    },
    { long_name: "EC2A 2BS", short_name: "EC2A 2BS", types: ["postal_code"] }
  ],
  adr_address:
    '<span class="street-address">25 Christopher St</span>, <span class="locality">London</span> <span class="postal-code">EC2A 2BS</span>, <span class="country-name">UK</span>',
  formatted_address: "25 Christopher St, London EC2A 2BS",
  geometry: {
    location: ({
      lat: 51.5214667,
      lng: -0.0837811999999758
    } as unknown) as google.maps.LatLng,
    viewport: ({
      south: 51.52001161970851,
      west: -0.08515613029146607,
      north: 51.5227095802915,
      east: -0.08245816970850228
    } as unknown) as google.maps.places.PlaceGeometry["viewport"]
  },
  icon: "https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png",
  id: "35b40554b389219b3bd7622a8162f164927fdc36",
  name: "25 Christopher St",
  place_id: "ChIJ93Kfaa4cdkgR-ElP4HhbwaQ",
  types: ["street_address"],
  url:
    "https://maps.google.com/?q=25+Christopher+St,+London+EC2A+2BS&ftid=0x48761cae699f72f7:0xa4c15b78e04f49f8",
  utc_offset: 60,
  html_attributions: []
}

export default place
