// tslint:disable object-literal-sort-keys
const data: google.maps.StreetViewPanoramaData = {
  copyright: "© 2019 Google",
  imageDate: "2018-02",
  location: {
    latLng: ({
      lat: 51.52165117136528,
      lng: -0.08375609276458817
    } as unknown) as google.maps.LatLng,
    shortDescription: "1 Dysart St",
    description: "1 Dysart St, London, England",
    pano: "D-xgirHjCRF_skmwHYFEvA"
  },
  links: [
    {
      description: "Dysart St",
      heading: 275.7821960449219,
      pano: "Thwe2S7bTMzSNSfqnkIejQ"
    },
    {
      description: "Dysart St",
      heading: 95.8537826538086,
      pano: "jaWkskxPZsXuuI1OAonLvA"
    }
  ],
  // eslint-disable-next-line @typescript-eslint/no-object-literal-type-assertion
  tiles: {
    centerHeading: 95.84429931640625,
    tileSize: { width: 512, height: 512 },
    worldSize: { width: 16384, height: 8192 }
  } as google.maps.StreetViewPanoramaData["tiles"]
}

export default data
