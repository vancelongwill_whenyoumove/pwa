import * as React from "react"

import { connect } from "react-redux"
import { signOut } from "../modules/auth/actions"
import { IState } from "../store/types"

import Button from "../components/Button"

interface IProps {
  user: string
  handleSignOut: () => void
}

export const Account: React.FunctionComponent<IProps> = ({
  user,
  handleSignOut
}) => (
  <div>
    <h1>My Account</h1>
    <p>Welcome, {user}!</p>
    <Button onClick={handleSignOut}>Sign Out</Button>
  </div>
)

function mapStateToProps(state: IState): { user: IState["auth"]["user"] } {
  return {
    user: state.auth.user
  }
}

const actionCreators = {
  handleSignOut: signOut
}

export default connect(
  mapStateToProps,
  actionCreators
)(Account)
