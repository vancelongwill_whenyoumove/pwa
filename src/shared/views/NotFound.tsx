import * as React from "react"

import styled from "@emotion/styled"

const Message = styled.p`
  font-size: 32px;
  color: red;
`
const NotFoundView: React.FunctionComponent = () => (
  <div>
    <Message>Page not found</Message>
  </div>
)

export default NotFoundView
