import * as React from "react"

import { connect, useDispatch } from "react-redux"
import styled from "@emotion/styled"

import GoogleMap, { MapOptions /* Maps */ } from "google-map-react"

import { GOOGLE_MAPS_API_KEY } from "../config/env"
import { colors } from "../config/theme"

import * as propertyActions from "../modules/properties/actions"

import { useGeolocation } from "../modules/geolocation/hooks"
import { usePlacesAutocomplete } from "../modules/placesautocomplete/hooks"

import { IState } from "../store/types"

import Button from "../components/Button"
import Card from "../components/Card"
import Flex from "../components/Flex"
import LocationIcon from "../components/LocationIcon"
import Overlay from "../components/Overlay"
import Image from "../components/Image"
import SearchInput from "../components/SearchInput"

type GMaps = typeof google.maps

const MapContainer = styled.div`
  height: 100vh;
  width: 100%;
`

function createMapOptions(/* _: Maps */): MapOptions {
  return {
    disableDefaultUI: true,
    mapTypeControl: false,
    panControl: false,
    scrollwheel: false,
    styles: [
      {
        stylers: [{ visibility: "on" }]
      }
    ]
  }
}

// this object gets serialised by google map react as url params on the script href
const bootstrapURLKeys = {
  key: GOOGLE_MAPS_API_KEY,
  language: "en",
  libraries: "places,geometry", // include the autocomplete client
  region: "gb"
}

const SearchContainer = styled.div`
  position: absolute;
  width: 100%;
  z-index: 3;
  padding-left: 2.5%;
  padding-right: 2.5%;
  transform: translateY(90vh);

  &:focus-within {
    transform: translateY(10vh);
  }

  transition: transform 1s ease-out;
`

const SearchListItem = styled.li`
  display: flex;
  align-items: center;
  font-size: 1rem;
  padding: 0.5rem;
  list-style: none;
  border-bottom: 1px solid lightgrey;

  & > svg {
    width: 1rem;
    margin-right: 1rem;
  }
`

const SearchList = styled.ul`
  background: rgba(255, 255, 255, 1);
  color: ${p => p.theme.colors.black};
  padding: 0.5rem;
  box-shadow: 0 1px 4px grey;

  /* stylelint-disable */
  ${SearchListItem}:last-child {
    border-bottom: none;
  }
  /* stylelint-enable */
`

// @TODO: use custom components for markers on the map?
// const Marker = styled.span<{ lat: number; lng: number }>`
//   position: absolute;
//   width: 40px;
//   height: 40px;
//   left: -20px;
//   top: -20px;
//   background-color: palevioletred;
// `

const Question = styled.span`
  color: ${p => p.theme.colors.white};
  font-size: 1.5rem;
`

interface IFormStep {
  prompt: string
  actions: string[]
}
const formSteps: IFormStep[] = [
  {
    prompt: "Are you selling this property?",
    actions: ["yes", "no"]
  },
  {
    prompt: "Are you buying this property?",
    actions: ["yes", "no"]
  }
]

export interface IProps {
  properties: IState["properties"]
}

export const MapView: React.FunctionComponent<IProps> = ({ properties }) => {
  const dispatch = useDispatch()
  const geoState = useGeolocation()
  const [formStep, setFormStep] = React.useState(0)
  const [query, setQuery] = React.useState("")

  const selectedLocation = properties[formStep ? "to" : "from"]
  const sessionToken = React.useRef<
    google.maps.places.AutocompleteSessionToken
  >()
  // Reference to the google map for placing markers etc
  const map = React.useRef<google.maps.Map | null>(null)

  const predictions = usePlacesAutocomplete(query)

  const center = geoState.position
    ? {
        lat: geoState.position.coords.latitude,
        lng: geoState.position.coords.longitude
      }
    : { lat: 51.5074, lng: 0.1278 }

  const selectedCoords =
    selectedLocation && selectedLocation.location
      ? selectedLocation.location
      : null

  const onSearchSuggestionSelect = React.useCallback(
    (prediction: google.maps.places.AutocompletePrediction) => {
      if (map.current === null) {
        return
      }
      let marker: google.maps.Marker | null = null
      const googlePlacesService = new google.maps.places.PlacesService(
        map.current
      )
      googlePlacesService.getDetails(
        {
          placeId: prediction.place_id,
          sessionToken: sessionToken.current
        },
        (details, status) => {
          if (status !== google.maps.places.PlacesServiceStatus.OK) {
            console.log("Error fetching place details: ", details, status)
          }
          if (details && details.geometry) {
            dispatch(
              propertyActions.setProperty(formStep ? "to" : "from", details)
            )

            if (map.current) {
              marker = new google.maps.Marker({
                // @TODO: optionally add label?
                // label: {
                //   color: "darkblue",
                //   // fontFamily:
                //   // fontSize:
                //   fontWeight: "700",
                //   text: selectedLocation.name
                // },
                map: map.current,
                position: details.geometry.location,
                title: details.name
              })
              // shift the map up a little so we can see the marker behind the centered card
              map.current.panBy(0, 100)
            }
          }
        }
      )

      return () => {
        // Delete marker
        if (marker !== null) {
          marker.setMap(null)
          marker = null
        }
      }
    },
    [formStep, dispatch]
  )

  const onLoadGoogle = React.useCallback(
    ({
      map: loadedMap
    }: // maps: loadedMaps
    {
      map: google.maps.Map
      maps: GMaps
    }) => {
      map.current = loadedMap
      if (!sessionToken.current) {
        sessionToken.current = new google.maps.places.AutocompleteSessionToken()
      }
    },
    []
  )

  const handleAnswerQuestion = React.useCallback(
    (value: string) => {
      const isYes = value === "yes"
      switch (formStep) {
        case 0:
          dispatch(
            propertyActions.updateProperty("from", {
              transactionType: isYes ? "sale" : "rental"
            })
          )
          setFormStep(formStep + 1)
          break
        case 1:
          dispatch(
            propertyActions.updateProperty("to", {
              transactionType: isYes ? "sale" : "rental"
            })
          )
          break
        default:
          break
      }
    },
    [formStep, dispatch]
  )

  return (
    <MapContainer>
      {!selectedCoords && (
        <SearchContainer>
          <SearchInput
            placeholder="Where are you moving from?"
            value={query}
            onKeyPress={e => {
              if (e.which === 13 || e.keyCode === 13) {
                setQuery("")
                onSearchSuggestionSelect(predictions[0])
              }
            }}
            onChange={e => {
              setQuery(e.target.value)
            }}
            onBlur={() => {
              // allow List.Item onClick to happen too
              setTimeout(() => setQuery(""))
            }}
          />
          {query.length > 2 && predictions.length > 0 && (
            <SearchList>
              {predictions.map(prediction => (
                <SearchListItem
                  key={prediction.place_id}
                  onClick={() => {
                    onSearchSuggestionSelect(prediction)
                  }}
                >
                  <LocationIcon />
                  {prediction.description}
                </SearchListItem>
              ))}
              <SearchListItem
                onClick={() => {
                  setQuery("")
                }}
              >
                ...or select manually on the map
              </SearchListItem>
            </SearchList>
          )}
        </SearchContainer>
      )}
      <GoogleMap
        bootstrapURLKeys={bootstrapURLKeys}
        zoom={17}
        options={createMapOptions}
        center={selectedCoords || center}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={onLoadGoogle}
      />
      {selectedLocation && selectedCoords && (
        <Overlay alwaysShow>
          <Overlay.Top>
            <Button
              color={colors.darkBlue}
              onClick={() =>
                dispatch(
                  propertyActions.setProperty(
                    formStep ? "to" : "from",
                    undefined
                  )
                )
              }
            >
              Choose another location
            </Button>
          </Overlay.Top>
          <Overlay.Center>
            <Card
              horizontal
              imageWidth="100px"
              image={<Image src={selectedLocation.imageURL} />}
            >
              <Card.Header>{selectedLocation.name}</Card.Header>
              <Card.Footer>
                {selectedLocation.address
                  .split(",")
                  .slice(1)
                  .join(",")}
              </Card.Footer>
            </Card>
          </Overlay.Center>
          <Overlay.Bottom>
            <Question>{formSteps[formStep].prompt}</Question>
            <Flex column>
              {formSteps[formStep].actions.map(action => (
                <Button
                  color={colors.lightBlue}
                  key={prompt + action}
                  value={action}
                  onClick={handleAnswerQuestion}
                >
                  {action}
                </Button>
              ))}
            </Flex>
          </Overlay.Bottom>
        </Overlay>
      )}
    </MapContainer>
  )
}

export function mapStateToProps(
  state: IState
): { properties: IState["properties"] } {
  return {
    properties: state.properties
  }
}

export default connect(mapStateToProps)(MapView)
