import * as React from "react"

import styled from "@emotion/styled"

import Button from "../components/Button"
import CaptureImage from "../components/CaptureImage"
import FilePicker from "../components/FilePicker"
import FileUpload from "../components/FileUpload"
import Flex from "../components/Flex"
import Overlay from "../components/Overlay"
import PageTitle from "../components/PageTitle"
import PhotoPreview from "../components/PhotoPreview"

const PickerContainer = styled.div`
  height: 300px;
`

const PreviewContainer = styled.div`
  width: 100%;
  max-height: 300px;
  overflow-y: scroll;
  overflow-x: hidden;

  & > * {
    margin-bottom: 10px;
  }
`

const FilePreviewContainer = styled.div`
  position: relative;
  cursor: pointer;
`

function shorten(fn: string): string {
  if (fn.length > 20) {
    const shortName = [fn.slice(0, 8), "...", fn.slice(fn.length - 8)].join("")
    return shortName
  }
  return fn
}

interface IUploadedFiles {
  [fileName: string]: {
    uploading: boolean
    uploaded: boolean
    errorMsg?: string
  }
}

export const Photo: React.FunctionComponent = () => {
  const [files, setFiles] = React.useState<File[]>([])
  const [uploadedFiles, setUploadedFiles] = React.useState<IUploadedFiles>({})

  const onUpload = React.useCallback(() => {
    setUploadedFiles(prevFiles => {
      const a = files.reduce((acc, f) => {
        if (acc[f.name] && acc[f.name].uploaded) {
          return acc
        }
        return {
          ...acc,
          [f.name]: {
            errorMsg: undefined,
            uploaded: false,
            uploading: true
          }
        }
      }, prevFiles)
      return a
    })
  }, [files])

  const onUploadDone = React.useCallback(
    (doneFile: File) =>
      setUploadedFiles(prevFiles => ({
        ...prevFiles,
        [doneFile.name]: {
          errorMsg: undefined,
          uploaded: true,
          uploading: false
        }
      })),
    []
  )

  const onUploadFail = React.useCallback((failFile: File, error: Error) => {
    setUploadedFiles(prevFiles => ({
      ...prevFiles,
      [failFile.name]: {
        errorMsg: error.message,
        uploaded: false,
        uploading: false
      }
    }))
  }, [])

  const removeFile = React.useCallback(
    clickedFile =>
      setFiles(prevFiles =>
        prevFiles.filter(
          f =>
            !(
              f.name === clickedFile.name &&
              f.lastModified === clickedFile.lastModified
            )
        )
      ),
    []
  )

  const updateFiles = React.useCallback(
    (newFiles: FileList) =>
      setFiles(prevFiles => [
        ...Array.from(newFiles).filter(
          item =>
            !prevFiles.find(
              pf =>
                item.name === pf.name && item.lastModified === pf.lastModified
            )
        ),
        ...prevFiles
      ]),
    []
  )

  const handleImageCapture = React.useCallback((blob: Blob | null) => {
    if (blob !== null) {
      setFiles(prevFiles => [
        new File([blob], `captured-image-${new Date().getTime()}.png`),
        ...prevFiles
      ])
    }
  }, [])

  return (
    <Flex column>
      <PageTitle>Photo upload</PageTitle>
      <span>Upload existing images</span>
      <br />
      <span>Files chosen: {files.length}</span>
      <Flex>
        <PickerContainer>
          <FilePicker
            multiple
            onFilesChange={updateFiles}
            accept="image/*,.pdf"
            capture="user"
            promptText="Drag an image in to the square or click here to choose from your files"
          />
        </PickerContainer>
      </Flex>
      <br />
      <Flex>
        <PreviewContainer>
          {files.length
            ? files.map(file => {
                const isUploaded =
                  uploadedFiles[file.name] && uploadedFiles[file.name].uploaded
                return (
                  <FilePreviewContainer
                    key={file.name + file.lastModified}
                    onClick={isUploaded ? undefined : () => removeFile(file)}
                  >
                    <PhotoPreview file={file} />
                    <Overlay>
                      <Overlay.Center>
                        {file.type !== "application/pdf"
                          ? `${shorten(file.name)} ${isUploaded ? "✓" : ""}`
                          : ""}
                      </Overlay.Center>
                      <Overlay.Bottom>
                        {isUploaded ? "Uploaded" : "Click to remove"}
                      </Overlay.Bottom>
                    </Overlay>
                  </FilePreviewContainer>
                )
              })
            : "Select some files to see their previews here"}
        </PreviewContainer>
      </Flex>
      <CaptureImage onImageCapture={handleImageCapture} />
      <Button disabled={!files.length} onClick={onUpload}>
        Upload photos
      </Button>
      <Flex column>
        {files.map(file => {
          if (!uploadedFiles[file.name]) {
            return
          }
          const key = file.name + file.lastModified
          const { uploaded, errorMsg } = uploadedFiles[file.name]
          if (uploaded) {
            return <div key={key}>Done.</div>
          }
          if (errorMsg) {
            return <div key={key}>An error occurred: {errorMsg}</div>
          }

          return (
            <FileUpload
              key={key}
              endpoint={"http://localhost:3000/upload"}
              onSuccess={onUploadDone}
              onFail={onUploadFail}
              file={file}
            />
          )
        })}
      </Flex>
    </Flex>
  )
}

export default Photo
