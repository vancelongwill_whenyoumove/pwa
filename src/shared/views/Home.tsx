import * as React from "react"

import { connect } from "react-redux"

import execEnv from "../lib/execEnv"

import Dog from "../components/Dog"
import Flex from "../components/Flex"

import { IDog } from "../modules/dogs/types"
import { IState } from "../store/types"

import * as dogsActions from "../modules/dogs/actions"

interface IProps {
  dogs: readonly IDog[]
  loadDog: (breed: string) => void
}

export class HomeView extends React.Component<IProps> {
  public constructor(props: IProps) {
    super(props)

    // (mis)-Use the constructor to trigger data fetching on the server side
    if (execEnv.isServer) {
      props.loadDog("samoyed")
    }
  }
  public componentDidMount(): void {
    // client-side data fetching
    if (!this.props.dogs.length) {
      this.props.loadDog("beagle")
    }
  }
  public render(): React.ReactNode {
    const { dogs } = this.props

    return (
      <Flex>
        {dogs.map(dog => (
          <Dog key={dog.id} url={dog.url} />
        ))}
      </Flex>
    )
  }
}

function mapStateToProps(state: IState): { dogs: readonly IDog[] } {
  return {
    dogs: state.dogs.dogs
  }
}

const actionCreators = {
  loadDog: dogsActions.loadDog
}

export default connect(
  mapStateToProps,
  actionCreators
)(HomeView)
