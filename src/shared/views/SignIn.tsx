import * as React from "react"

import { connect } from "react-redux"

import { IState } from "../store/types"

import * as authActions from "../modules/auth/actions"

import Flex from "../components/Flex"
import PageTitle from "../components/PageTitle"
import SignIn from "../components/SignIn"

interface IProps {
  signInError: false | string
  handleSignIn: ({ user, password }: { user: string; password: string }) => void
}

const SignInView: React.FunctionComponent<IProps> = ({
  signInError,
  handleSignIn
}) => (
  <Flex>
    <PageTitle>Sign in</PageTitle>
    <SignIn onSubmit={handleSignIn} />
    {signInError && <div>{signInError}</div>}
  </Flex>
)

function mapStateToProps(
  state: IState
): { signInError: IProps["signInError"] } {
  return {
    signInError: state.auth.signInError
  }
}

const actionCreators = {
  handleSignIn: authActions.signIn
}

export default connect(
  mapStateToProps,
  actionCreators
)(SignInView)
