import * as React from "react"

import { connect, useDispatch } from "react-redux"
import styled from "@emotion/styled"

import { IState } from "../store/types"
import { colors } from "../config/theme"

import execEnv from "../lib/execEnv"

import { IProperty } from "../modules/properties/types"
import * as propertiesActions from "../modules/properties/actions"

import Card from "../components/Card"
import PageTitle from "../components/PageTitle"
import ProgressCircle from "../components/ProgressCircle"
import Flex from "../components/Flex"

const ViewContainer = styled.div`
  background-color: ${p => p.theme.colors.white};
`
const StatusContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${p => p.theme.colors.lightBlue};
  border-radius: 0 0 1rem 1rem;

  & > p {
    margin-bottom: 0.5rem;
    margin-top: 0.5rem;
    font-size: 1.2rem;
  }

  * {
    color: ${p => p.theme.colors.white};
    z-index: 1;
  }
`

const Section = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
`

const ListSection = styled(Flex)`
  width: 90%;
  margin: 0 auto;
  margin-bottom: 1rem;

  & > * {
    margin-top: 1rem;
  }
`

const ProgressSection = styled(Section)`
  width: 70%;
  justify-content: space-between;

  & > p {
    padding-left: 0.5rem;
    font-size: 1.5rem;
  }
`

export function getAddressComponents(
  commaDelimitedAddress: string
): { firstLine: string; rest: string } {
  const addressComponents = commaDelimitedAddress.split(",")
  if (addressComponents.length < 2) {
    throw new Error("Expected a comma delimited address!")
  }
  return {
    firstLine: addressComponents[0],
    rest: addressComponents.slice(1).join(",")
  }
}

export interface IProps {
  to: IProperty
  from: IProperty
}

export const Dashboard: React.FunctionComponent<IProps> = ({ to, from }) => {
  const dispatch = useDispatch()

  if (execEnv.isServer || (!to.name && !from.name)) {
    dispatch(propertiesActions.fetchUserProperties())
  }

  let toAddress: ReturnType<typeof getAddressComponents>
  let fromAddress: ReturnType<typeof getAddressComponents>
  try {
    toAddress = getAddressComponents(to.address)
    fromAddress = getAddressComponents(from.address)
  } catch (e) {
    return <p>Choose your properties first!</p>
  }

  return (
    <ViewContainer>
      <StatusContainer>
        <PageTitle>Hi Max</PageTitle>
        <ProgressSection>
          <ProgressCircle
            strokeColor={colors.white}
            textColor={colors.white}
            percentage={35}
            backgroundColor="transparent"
          />
          <p>of the way through your home move</p>
        </ProgressSection>
        <p>Moving from</p>
        <Card
          image={from.imageURL}
          imageWidth="90px"
          horizontal
          alignRight
          tail="left"
        >
          <Card.Header>{fromAddress.firstLine}</Card.Header>
          <Card.Footer>{fromAddress.rest}</Card.Footer>
        </Card>
        <p>to</p>
        <Card image={to.imageURL} imageWidth="90px" horizontal tail="right">
          <Card.Header>{toAddress.firstLine}</Card.Header>
          <Card.Footer>{toAddress.rest}</Card.Footer>
        </Card>
        <br />
      </StatusContainer>

      <ListSection column>
        <Card horizontal>
          <Card.Header>Something to do!</Card.Header>
          <Card.Body>A description here</Card.Body>
          <Card.Footer>Some footer text</Card.Footer>
        </Card>

        <Card horizontal>
          <Card.Header>Something to do!</Card.Header>
          <Card.Body>A description here</Card.Body>
          <Card.Footer>Some footer text</Card.Footer>
        </Card>

        <Card horizontal>
          <Card.Header>Something to do!</Card.Header>
          <Card.Body>A description here</Card.Body>
          <Card.Footer>Some footer text</Card.Footer>
        </Card>
      </ListSection>
    </ViewContainer>
  )
}

export function mapStateToProps(state: IState): IState["properties"] {
  return state.properties
}
export default connect(mapStateToProps)(Dashboard)
