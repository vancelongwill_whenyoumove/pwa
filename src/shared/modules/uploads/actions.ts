// tslint:disable object-literal-sort-keys

export const FILE_UPLOAD = "FILE_UPLOAD"
export interface IFileUploadAction {
  type: typeof FILE_UPLOAD
}
export function fileUpload(): IFileUploadAction {
  return {
    type: FILE_UPLOAD
  }
}

export const FILE_UPLOAD_SUCCESS = "FILE_UPLOAD_SUCCESS"
export interface IFileUploadSuccessAction {
  type: typeof FILE_UPLOAD_SUCCESS
}
export function fileUploadSuccess(): IFileUploadSuccessAction {
  return {
    type: FILE_UPLOAD_SUCCESS
  }
}

export const FILE_UPLOAD_ERROR = "FILE_UPLOAD_ERROR"
export interface IFileUploadErrorAction {
  type: typeof FILE_UPLOAD_ERROR
}
export function fileUploadError(): IFileUploadErrorAction {
  return {
    type: FILE_UPLOAD_ERROR
  }
}

export const FILE_UPLOAD_CANCEL = "FILE_UPLOAD_CANCEL"
export interface IFileUploadCancelAction {
  type: typeof FILE_UPLOAD_CANCEL
}
export function fileUploadCancel(): IFileUploadCancelAction {
  return {
    type: FILE_UPLOAD_CANCEL
  }
}

export const FILE_UPLOAD_PROGRESS = "FILE_UPLOAD_PROGRESS"
export interface IFileUploadProgressAction {
  type: typeof FILE_UPLOAD_PROGRESS
  payload: {
    progress: number
  }
}
export function fileUploadProgress(
  progress: number
): IFileUploadProgressAction {
  return {
    type: FILE_UPLOAD_PROGRESS,
    payload: { progress }
  }
}

export type ActionTypes =
  | IFileUploadErrorAction
  | IFileUploadSuccessAction
  | IFileUploadAction
  | IFileUploadCancelAction
  | IFileUploadProgressAction
