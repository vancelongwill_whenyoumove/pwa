import { CancelTokenSource } from "axios"
export interface IState {
  loading: boolean
  error: boolean
  done: boolean
  progress: number
  source: CancelTokenSource
}
