import {
  ActionTypes,
  FILE_UPLOAD,
  FILE_UPLOAD_CANCEL,
  FILE_UPLOAD_ERROR,
  FILE_UPLOAD_PROGRESS,
  FILE_UPLOAD_SUCCESS
} from "./actions"
import { IState } from "./types"

export function uploadsReducer(state: IState, action: ActionTypes): IState {
  switch (action.type) {
    case FILE_UPLOAD:
      return {
        ...state,
        done: false,
        error: false,
        loading: true
      }
    case FILE_UPLOAD_SUCCESS:
      return {
        ...state,
        done: true,
        error: false,
        loading: false
      }
    case FILE_UPLOAD_ERROR:
      return {
        ...state,
        done: false,
        error: true,
        loading: false
      }
    case FILE_UPLOAD_CANCEL:
      return {
        ...state,
        done: false,
        error: false,
        loading: false
      }
    case FILE_UPLOAD_PROGRESS:
      return {
        ...state,
        progress: action.payload.progress
      }
    default:
      return state
  }
}

export default uploadsReducer
