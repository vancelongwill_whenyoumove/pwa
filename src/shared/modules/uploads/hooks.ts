import * as React from "react"

import axios from "axios"

import {
  fileUpload,
  fileUploadCancel,
  fileUploadError,
  fileUploadProgress,
  fileUploadSuccess
} from "./actions"
import uploadsReducer from "./reducer"
import { IState } from "./types"

export interface IUploadOptions {
  file: File
  endpoint: string
  onSuccess?: (file: File) => void
  onFail?: (file: File, error: Error) => void
}

export function useFileUpload({
  file,
  endpoint,
  onSuccess,
  onFail
}: IUploadOptions): IState {
  const [state, dispatch] = React.useReducer(uploadsReducer, {
    done: false,
    error: false,
    loading: false,
    progress: 0,
    source: axios.CancelToken.source()
  })

  React.useEffect(() => {
    if (!state.loading) {
      dispatch(fileUpload())
      const data = new FormData()
      data.append("file", file)
      axios
        .put(endpoint, data, {
          cancelToken: state.source.token,
          onUploadProgress: progressEvent => {
            const percentCompleted = Math.round(
              (progressEvent.loaded * 100) / progressEvent.total
            )
            dispatch(fileUploadProgress(percentCompleted))
          }
        })
        .then(() => {
          dispatch(fileUploadSuccess())
          if (onSuccess) {
            onSuccess(file)
          }
        })
        .catch(err => {
          if (axios.isCancel(err)) {
            dispatch(fileUploadCancel())
            return
          }
          dispatch(fileUploadError())
          if (onFail) {
            onFail(file, err)
          }
        })
    }
  }, [endpoint, file, onFail, onSuccess, state.loading, state.source.token])

  return state
}

export default useFileUpload
