import * as React from "react"

export function usePlacesAutocomplete(
  query: string,
  sessionToken?: string
): google.maps.places.AutocompletePrediction[] {
  const [predictions, setPredictions] = React.useState<
    google.maps.places.AutocompletePrediction[]
  >([])

  React.useEffect(() => {
    if (typeof google === "undefined" || !query) {
      return
    }
    const autocomplete = new google.maps.places.AutocompleteService()
    autocomplete.getPlacePredictions(
      {
        componentRestrictions: {
          country: ["uk"]
        },
        types: ["address"],
        input: query,
        sessionToken
      },
      (results, status) => {
        if (status !== google.maps.places.PlacesServiceStatus.OK) {
          console.log(
            `An error occured while fetching autocomplete predictions: `,
            results,
            status
          )
          return
        }
        setPredictions(results)
      }
    )
  }, [query, sessionToken])

  return predictions
}
