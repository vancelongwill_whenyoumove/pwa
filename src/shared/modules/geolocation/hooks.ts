import * as React from "react"

import execEnv from "../../lib/execEnv"

interface IState {
  supported?: boolean
  timedOut?: boolean
  position?: Position
  error: false | string
}

export function useGeolocation(): IState {
  const [state, setState] = React.useState<IState>({
    error: false,
    supported: true
  })

  const onSuccess = React.useCallback((position: Position) => {
    setState(prevState => ({ ...prevState, position }))
  }, [])

  const onError = React.useCallback((error: PositionError) => {
    if (error.code === error.TIMEOUT) {
      setState(prevState => ({ ...prevState, timedOut: true }))
    }
    setState(prevState => ({ ...prevState, error: error.message }))
  }, [])

  React.useEffect(() => {
    navigator.geolocation.getCurrentPosition(onSuccess, onError, {
      timeout: 15 * 1000
    })
  }, [onError, onSuccess])

  if (execEnv.isServer) {
    return state
  }

  if (!execEnv.isGeolocationSupported) {
    return { supported: false, error: false }
  }

  return state
}
