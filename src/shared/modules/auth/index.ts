import reducer from "./reducers"
import { watchAuth as saga } from "./sagas"
import { IState } from "./types"
export { reducer, IState, saga }
