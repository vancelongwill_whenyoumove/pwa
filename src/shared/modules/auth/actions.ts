// tslint:disable object-literal-sort-keys
export const SIGN_IN_REQUEST = "auth/SIGN_IN_REQUEST"
export interface ISignInRequest {
  type: typeof SIGN_IN_REQUEST
  payload: {
    user: string
    password: string
  }
}
export function signIn(payload: ISignInRequest["payload"]): ISignInRequest {
  return {
    type: SIGN_IN_REQUEST,
    payload
  }
}

export const SIGN_IN_ERROR = "auth/SIGN_IN_ERROR"
export interface ISignInError {
  type: typeof SIGN_IN_ERROR
  payload: {
    error: string
  }
  error: true
}
export function signInError(payload: ISignInError["payload"]): ISignInError {
  return {
    type: SIGN_IN_ERROR,
    payload,
    error: true
  }
}

export const SIGN_IN_SUCCESS = "auth/SIGN_IN_SUCCESS"
export interface ISignInSuccess {
  type: typeof SIGN_IN_SUCCESS
  payload: {
    user: string
    token: string
  }
}
export function signInSuccess(
  payload: ISignInSuccess["payload"]
): ISignInSuccess {
  return {
    type: SIGN_IN_SUCCESS,
    payload
  }
}

export const REGISTER_REQUEST = "auth/REGISTER_REQUEST"
export interface IRegisterRequest {
  type: typeof REGISTER_REQUEST
  payload: {
    user: string
    password: string
  }
}
export function register(
  payload: IRegisterRequest["payload"]
): IRegisterRequest {
  return {
    type: REGISTER_REQUEST,
    payload
  }
}

export const REGISTER_ERROR = "auth/REGISTER_ERROR"
export interface IRegisterError {
  type: typeof REGISTER_ERROR
  payload: {
    error: string
  }
  error: true
}
export function registerError(
  payload: IRegisterError["payload"]
): IRegisterError {
  return {
    type: REGISTER_ERROR,
    payload,
    error: true
  }
}
export const REGISTER_SUCCESS = "auth/REGISTER_SUCCESS"
export interface IRegisterSuccess {
  type: typeof REGISTER_SUCCESS
  payload: {
    user: string
    token: string
  }
}
export function registerSuccess(
  payload: IRegisterSuccess["payload"]
): IRegisterSuccess {
  return {
    type: REGISTER_SUCCESS,
    payload
  }
}

export const SIGN_OUT = "auth/SIGN_OUT"
export interface ISignOut {
  type: typeof SIGN_OUT
}
export function signOut(): ISignOut {
  return {
    type: SIGN_OUT
  }
}

export type ActionTypes =
  | ISignInRequest
  | ISignInError
  | ISignInSuccess
  | IRegisterRequest
  | IRegisterError
  | IRegisterSuccess
  | ISignOut
