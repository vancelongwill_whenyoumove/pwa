/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { call, put, takeLatest } from "redux-saga/effects"
import Auth from "../../lib/auth"
import {
  IRegisterRequest,
  ISignInRequest,
  REGISTER_REQUEST,
  registerError,
  registerSuccess,
  SIGN_IN_REQUEST,
  signInError,
  signInSuccess
} from "./actions"

export function* handleSignIn(action: ISignInRequest) {
  try {
    const res = yield call(Auth.signIn, action.payload)
    yield put(signInSuccess({ user: res.data.user, token: res.data.token }))
  } catch (e) {
    yield put(signInError({ error: e.message }))
  }
}

export function* handleRegister(action: IRegisterRequest) {
  try {
    const res = yield call(Auth.register, action.payload)
    yield put(registerSuccess({ user: res.data.user, token: res.data.token }))
  } catch (e) {
    yield put(registerError({ error: e.message }))
  }
}

export function* watchAuth() {
  yield takeLatest(SIGN_IN_REQUEST, handleSignIn)
  yield takeLatest(REGISTER_REQUEST, handleRegister)
}
