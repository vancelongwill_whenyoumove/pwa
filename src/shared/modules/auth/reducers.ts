import { PersistConfig, persistReducer } from "redux-persist"
import storage from "redux-persist/lib/storage" // defaults to localStorage for web

import {
  ActionTypes,
  REGISTER_ERROR,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  SIGN_IN_ERROR,
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_OUT
} from "./actions"
import { IState } from "./types"

const initialState: IState = {
  isAuthenticated: false,
  loading: false,
  registerError: false,
  signInError: false,
  token: "",
  user: ""
}

export function auth(
  state: IState = initialState,
  action: ActionTypes
): IState {
  switch (action.type) {
    case REGISTER_REQUEST:
    case SIGN_IN_REQUEST:
      return {
        ...state,
        loading: true
      }
    case REGISTER_ERROR:
      return {
        ...state,
        registerError: action.payload.error
      }
    case SIGN_IN_ERROR:
      return {
        ...state,
        signInError: action.payload.error
      }
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        signInError: false,
        token: action.payload.token,
        user: action.payload.user
      }
    case REGISTER_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        registerError: false,
        token: action.payload.token,
        user: action.payload.user
      }
    case SIGN_OUT:
      return initialState
    default:
      return state
  }
}

const persistConfig: PersistConfig = {
  blacklist: ["signInError"],
  key: "root",
  storage
}

export default persistReducer(persistConfig, auth)
