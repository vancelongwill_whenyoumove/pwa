export interface IState {
  readonly isAuthenticated: boolean
  readonly user: string
  readonly token: string
  readonly signInError: false | string
  readonly registerError: false | string
  readonly loading: boolean
}
