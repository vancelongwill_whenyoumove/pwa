import {
  ActionTypes,
  FILE_LOAD,
  FILE_LOAD_ERROR,
  FILE_LOAD_SUCCESS
} from "./actions"
import { IFilesState } from "./types"

export function filesReducer(
  state: IFilesState,
  action: ActionTypes
): IFilesState {
  switch (action.type) {
    case FILE_LOAD:
      return {
        ...state,
        loading: true
      }
    case FILE_LOAD_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false
      }
    case FILE_LOAD_ERROR:
      return {
        ...state,
        error: true,
        loading: false
      }
    default:
      return state
  }
}

export default filesReducer
