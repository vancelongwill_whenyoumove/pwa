export interface IFilesState {
  data: string
  loading: boolean
  error: boolean
}
