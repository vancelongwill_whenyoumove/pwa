import * as React from "react"

import { fileLoad, fileLoadError, fileLoadSuccess } from "./actions"
import filesReducer from "./reducer"
import { IFilesState } from "./types"

export function useFileLoad(file: File): IFilesState {
  const [state, dispatch] = React.useReducer(filesReducer, {
    data: "",
    error: false,
    loading: false
  })

  React.useEffect(() => {
    if (!state.data && !state.loading) {
      dispatch(fileLoad())
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => {
        if (typeof reader.result === "string") {
          dispatch(fileLoadSuccess(reader.result))
        } else {
          // tslint:disable-next-line: no-console
          console.warn(`Expected a string result while reading file`)
          dispatch(fileLoadError())
        }
      }
    }
  }, [file, state.data, state.loading])

  return state
}
