// tslint:disable object-literal-sort-keys

export const FILE_LOAD = "FILE_LOAD"
export interface IFileLoadAction {
  type: typeof FILE_LOAD
}
export function fileLoad(): IFileLoadAction {
  return {
    type: FILE_LOAD
  }
}

export const FILE_LOAD_SUCCESS = "FILE_LOAD_SUCCESS"
export interface IFileLoadSuccessAction {
  type: typeof FILE_LOAD_SUCCESS
  payload: {
    data: string
  }
}
export function fileLoadSuccess(data: string): IFileLoadSuccessAction {
  return {
    type: FILE_LOAD_SUCCESS,
    payload: { data }
  }
}

export const FILE_LOAD_ERROR = "FILE_LOAD_ERROR"
export interface IFileLoadErrorAction {
  type: typeof FILE_LOAD_ERROR
}
export function fileLoadError(): IFileLoadErrorAction {
  return {
    type: FILE_LOAD_ERROR
  }
}
export type ActionTypes =
  | IFileLoadErrorAction
  | IFileLoadSuccessAction
  | IFileLoadAction
