export type PropertyTransactionType = "rental" | "sale"
export type MoveDirection = "from" | "to"

export interface IProperty {
  name: string
  address: string
  imageURL: string
  location?: {
    lat: number
    lng: number
  }
  transactionType: PropertyTransactionType
  // ...more fields with user submitted info about property
}

export interface IState {
  from: IProperty
  to: IProperty
}
