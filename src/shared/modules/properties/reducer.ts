import {
  PersistConfig,
  persistReducer,
  REHYDRATE,
  RehydrateAction as IRehydrateActionES
} from "redux-persist"
import storage from "redux-persist/lib/storage" // defaults to localStorage for web

import execEnv from "../../lib/execEnv"

import { IState, IProperty, PropertyTransactionType } from "./types"
import {
  FETCH_USER_PROPERTIES_SUCCESS,
  SET_PROPERTY,
  UPDATE_PROPERTY,
  ActionTypes
} from "./actions"
import { getPropertyInfo } from "./utils"

interface IRehydrateAction extends Omit<IRehydrateActionES, "type"> {
  type: typeof REHYDRATE
}

const defaultTransactionType: PropertyTransactionType = "sale"
const persistKey = "properties"

export function getInitialPropertyState(): IProperty {
  return {
    name: "",
    address: "",
    imageURL: "",
    transactionType: defaultTransactionType
  }
}

export function isStatePrepolulated(state: IState): boolean {
  return Boolean(
    (state && state.to && state.to.name) ||
      (state && state.from && state.from.name)
  )
}

export const initialState = {
  from: getInitialPropertyState(),
  to: getInitialPropertyState()
}

export function properties(
  state: IState = initialState,
  action: ActionTypes | IRehydrateAction
): IState {
  switch (action.type) {
    case FETCH_USER_PROPERTIES_SUCCESS:
      return {
        ...state,
        ...action.payload
      }
    case SET_PROPERTY:
      return {
        ...state,
        [action.payload.direction]: action.payload.place
          ? getPropertyInfo(action.payload.place)
          : getInitialPropertyState()
      }
    case UPDATE_PROPERTY:
      return {
        ...state,
        [action.payload.direction]: {
          ...state[action.payload.direction],
          ...action.payload.property
        }
      }
    case REHYDRATE:
      // intercept the REHYDRATE action as a workaround for https://github.com/rt2zz/redux-persist/issues/189
      if (
        !execEnv.isServer &&
        action.payload &&
        action.payload.key &&
        action.payload.key === persistKey
      ) {
        return isStatePrepolulated(state) ? state : action.payload
      }
      return state
    default:
      return state
  }
}

const persistConfig: PersistConfig = {
  key: persistKey,
  storage
}

export default persistReducer(persistConfig, properties)
