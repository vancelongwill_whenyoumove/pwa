/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { call, put, takeLatest } from "redux-saga/effects"

import { IProperty } from "./types"
import {
  SET_PROPERTY,
  FETCH_USER_PROPERTIES,
  ISetProperty,
  updateProperty,
  fetchUserPropertiesSuccess
} from "./actions"
import { getPlaceImage } from "../placeimage/utils"

export function* handleSetProperty(action: ISetProperty) {
  if (action.payload.place) {
    const imageURL = yield call(getPlaceImage, action.payload.place)
    yield put(updateProperty(action.payload.direction, { imageURL }))
  }
}

const mockProperties = {
  from: {
    address: "25 Christopher St, London EC2A 2BS",
    name: "25 Christopher St",
    location: { lat: 51.5214667, lng: -0.0837811999999758 },
    imageURL:
      "https://maps.googleapis.com/maps/api/streetview?location=25%20Christopher%20St,%20London%20EC2A%202BS&heading=4.840616978630692&fov=120&size=300x300&key=AIzaSyB727-8V17k4FyPrfdjwu0V7iTvRm7gReU",
    transactionType: "sale"
  },
  to: {
    address: "25 Chorlton St, Stretford, Manchester M16 9HN",
    name: "25 Chorlton St",
    location: { lat: 53.46377899999999, lng: -2.268600300000003 },
    imageURL:
      "https://maps.googleapis.com/maps/api/streetview?location=25%20Chorlton%20St,%20Stretford,%20Manchester%20M16%209HN&heading=-86.70276624938481&fov=120&size=300x300&key=AIzaSyB727-8V17k4FyPrfdjwu0V7iTvRm7gReU",
    transactionType: "rental"
  }
}

function mockFetchProperties(): Promise<{ to: IProperty; from: IProperty }> {
  return new Promise(resolve => resolve(mockProperties as any))
}

export function* handleFetchProperties() {
  const properties = yield call(mockFetchProperties)

  yield put(fetchUserPropertiesSuccess(properties))
}

export function* watchProperties() {
  yield takeLatest(SET_PROPERTY, handleSetProperty)
  yield takeLatest(FETCH_USER_PROPERTIES, handleFetchProperties)
}
