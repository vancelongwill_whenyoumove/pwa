import { IProperty } from "./types"

export function getPropertyInfo(
  place: google.maps.places.PlaceResult
): Partial<IProperty> {
  const location =
    place.geometry && place.geometry.location
      ? {
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng()
        }
      : undefined
  return {
    address: place.formatted_address,
    name: place.name,
    location
  }
}
