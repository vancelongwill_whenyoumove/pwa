import { IProperty, MoveDirection } from "./types"

export const FETCH_USER_PROPERTIES = "properties/FETCH_USER_PROPERTIES"
export interface IFetchUserProperties {
  type: typeof FETCH_USER_PROPERTIES
}
export function fetchUserProperties(): IFetchUserProperties {
  return {
    type: FETCH_USER_PROPERTIES
  }
}

export const FETCH_USER_PROPERTIES_SUCCESS =
  "properties/FETCH_USER_PROPERTIES_SUCCESS"
export interface IFetchUserPropertiesSuccess {
  type: typeof FETCH_USER_PROPERTIES_SUCCESS
  payload: {
    to: IProperty
    from: IProperty
  }
}
export function fetchUserPropertiesSuccess(
  properties: IFetchUserPropertiesSuccess["payload"]
): IFetchUserPropertiesSuccess {
  return {
    type: FETCH_USER_PROPERTIES_SUCCESS,
    payload: properties
  }
}

export const FETCH_USER_PROPERTIES_FAIL =
  "properties/FETCH_USER_PROPERTIES_FAIL"
export interface IFetchUserPropertiesFail {
  type: typeof FETCH_USER_PROPERTIES_FAIL
}
export function fetchUserPropertiesFail(): IFetchUserPropertiesFail {
  return {
    type: FETCH_USER_PROPERTIES_FAIL
  }
}

export const SET_PROPERTY = "properties/SET_PROPERTY"
export interface ISetProperty {
  type: typeof SET_PROPERTY
  payload: {
    direction: MoveDirection
    place: google.maps.places.PlaceResult | undefined
  }
}
export function setProperty(
  direction: MoveDirection,
  place: google.maps.places.PlaceResult | undefined
): ISetProperty {
  return {
    type: SET_PROPERTY,
    payload: {
      direction,
      place
    }
  }
}

export const UPDATE_PROPERTY = "properties/UPDATE_PROPERTY"
export interface IUpdateProperty {
  type: typeof UPDATE_PROPERTY
  payload: {
    direction: MoveDirection
    property: Partial<IProperty>
  }
}
export function updateProperty(
  direction: MoveDirection,
  property: Partial<IProperty>
): IUpdateProperty {
  return {
    type: UPDATE_PROPERTY,
    payload: {
      direction,
      property
    }
  }
}

export type ActionTypes =
  | IUpdateProperty
  | ISetProperty
  | IFetchUserProperties
  | IFetchUserPropertiesFail
  | IFetchUserPropertiesSuccess
