import {
  ActionTypes,
  LOAD_DOG,
  LOAD_DOG_ERROR,
  LOAD_DOG_SUCCESS
} from "./actions"
import { IState } from "./types"

const initialState: IState = {
  dogs: [],
  error: false,
  loading: false
}

export function dogs(
  state: IState = initialState,
  action: ActionTypes
): IState {
  switch (action.type) {
    case LOAD_DOG:
      return {
        ...state,
        loading: true
      }
    case LOAD_DOG_ERROR:
      return {
        ...state,
        error: action.payload.error
      }

    case LOAD_DOG_SUCCESS:
      return {
        ...state,
        dogs: [...state.dogs, action.payload]
      }
    default:
      return state
  }
}

export default dogs
