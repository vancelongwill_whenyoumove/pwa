export interface IDog {
  readonly url: string
  readonly id: string
}

export interface IState {
  readonly dogs: readonly IDog[]
  readonly error: false | string
  readonly loading: boolean
}
