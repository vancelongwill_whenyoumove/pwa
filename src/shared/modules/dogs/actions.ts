// tslint:disable object-literal-sort-keys
import { IDog } from "./types"

export const LOAD_DOG = "dogs/LOAD_DOG"
export interface ILoadDog {
  type: typeof LOAD_DOG
  payload: string
}
export function loadDog(breed: string): ILoadDog {
  return {
    type: LOAD_DOG,
    payload: breed
  }
}

export const LOAD_DOG_ERROR = "dogs/LOAD_DOG_ERROR"
export interface ILoadDogError {
  type: typeof LOAD_DOG_ERROR
  payload: { error: string }
  error: true
}
export function loadDogError(error: string): ILoadDogError {
  return {
    type: LOAD_DOG_ERROR,
    payload: { error },
    error: true
  }
}

export const LOAD_DOG_SUCCESS = "dogs/LOAD_DOG_SUCCESS"
export interface ILoadDogSuccess {
  type: typeof LOAD_DOG_SUCCESS
  payload: IDog
}
export function loadDogSuccess(dog: IDog): ILoadDogSuccess {
  return {
    type: LOAD_DOG_SUCCESS,
    payload: dog
  }
}

export type ActionTypes = ILoadDog | ILoadDogError | ILoadDogSuccess
