/* eslint-disable @typescript-eslint/explicit-function-return-type */
import axios from "axios"
import { call, put, takeEvery } from "redux-saga/effects"
import { ILoadDog, LOAD_DOG, loadDogError, loadDogSuccess } from "./actions"

export function* handleLoadDog(action: ILoadDog) {
  try {
    const res = yield call(
      axios.get,
      `https://api.thedogapi.com/v1/images/search?q=${action.payload}`
    )
    yield put(
      loadDogSuccess({
        ...res.data[0],
        url: "https://source.unsplash.com/random/600x400"
      })
    )
  } catch (e) {
    yield put(loadDogError(e.message))
  }
}

export function* watchDogs() {
  yield takeEvery(LOAD_DOG, handleLoadDog)
}
