import { GOOGLE_MAPS_API_KEY } from "../../config/env"

export function getPanoramaByLocationPromise(
  latlng: google.maps.LatLng | google.maps.LatLngLiteral,
  radius: number
): Promise<google.maps.StreetViewPanoramaData> {
  return new Promise((resolve, reject) => {
    const streetviewService = new google.maps.StreetViewService()
    streetviewService.getPanoramaByLocation(latlng, radius, (data, status) => {
      if (
        status !== google.maps.StreetViewStatus.OK ||
        !data ||
        !data.location
      ) {
        reject(status)
      } else if (data) {
        resolve(data)
      } else {
        reject("No data")
      }
    })
  })
}

export async function getPlaceImage(
  place: google.maps.places.PlaceResult,
  width: number = 300,
  height: number = 300
): Promise<string> {
  if (!place.geometry || !place.formatted_address) {
    throw new Error(
      "Expected `geometry` and `formatted_address` properties to be present on PlaceResult"
    )
  }
  try {
    const data = await getPanoramaByLocationPromise(
      place.geometry.location,
      100
    )
    if (!data.location) {
      throw new Error("No location in data")
    }
    const locationTo = data.location.latLng
    if (!locationTo) {
      throw new Error("No location in data")
    }
    const locationFrom = place.geometry.location

    const heading = google.maps.geometry.spherical.computeHeading(
      locationFrom,
      locationTo
    )
    const url = `https://maps.googleapis.com/maps/api/streetview?location=${place.formatted_address.replace(
      / /g,
      "%20"
    )}&heading=${heading}&fov=120&size=${height}x${width}&key=${GOOGLE_MAPS_API_KEY}`

    return url
  } catch (e) {
    const mapUrl = `https://maps.googleapis.com/maps/api/staticmap?&zoom=17&size=${height}x${width}&maptype=roadmap&markers=color:blue|${place.formatted_address.replace(
      / /g,
      "+"
    )}&key=${GOOGLE_MAPS_API_KEY}`
    return mapUrl
  }
}
