import * as React from "react"

import { getPlaceImage } from "./utils"

interface IProps {
  place?: google.maps.places.PlaceResult
  width?: number
  height?: number
}

export const usePlaceImage = ({ place, height, width }: IProps): string => {
  const [url, setURL] = React.useState("")

  React.useEffect(() => {
    if (place) {
      getPlaceImage(place, width, height)
        .then(url => {
          setURL(url)
        })
        .catch((e: Error) => {
          console.log("Unable to generate place image url", e.message)
        })
    }
  }, [place, height, width])

  return url
}
