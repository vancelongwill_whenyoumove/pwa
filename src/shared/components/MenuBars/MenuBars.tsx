import * as React from "react"

import styled from "@emotion/styled"

const MenuBarsContainer = styled.div`
  position: absolute;
  top: 10px;
  left: 10px;
  z-index: 1;
`

const Bar = styled.div`
  width: 35px;
  height: 5px;
  background-color: ${p => p.color};
  margin: 6px 0;
`

interface IProps {
  onClick: () => void
  color?: string
}

export const MenuBars: React.FunctionComponent<IProps> = ({
  onClick,
  color = "white"
}) => {
  return (
    <MenuBarsContainer onClick={onClick}>
      <Bar color={color} />
      <Bar color={color} />
      <Bar color={color} />
    </MenuBarsContainer>
  )
}
