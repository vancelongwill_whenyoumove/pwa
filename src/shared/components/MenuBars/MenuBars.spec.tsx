import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { MenuBars } from "./MenuBars"

describe("MenuBars", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<MenuBars onClick={jest.fn()} />)).toMatchSnapshot()
  })
})
