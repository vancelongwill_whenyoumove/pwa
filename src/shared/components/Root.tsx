// This component serves as root of our react app (it will also be rendered on the server side), client side specific initialisation should be done in `src/client/index.tsx`
import * as React from "react"
import { Route, Switch } from "react-router-dom"

import { css, Global } from "@emotion/core"
import styled from "@emotion/styled"
import { ThemeProvider } from "emotion-theming"

import theme from "../config/theme"

import PrivateRoute from "../components/PrivateRoute"
import routes from "../routes"

import NotFound from "../views/NotFound"
import Nav from "./Nav"

import { fontFace } from "../config/font"

const AppContainer = styled.div`
  margin: 0;
  min-height: 100vh;
  max-width: 100vw;
  overflow-x: hidden;
  background-color: ${p => p.theme.colors.lightGrey};
  z-index: -1;
`

const globalCSS = css`
  * {
    box-sizing: border-box;
    font-family: "SF Display", -apple-system, BlinkMacSystemFont, sans-serif;
    color: ${theme.colors.black};
    font-weight: 200;
  }

  ${fontFace}
`

export const Root: React.FunctionComponent = () => (
  <ThemeProvider theme={theme}>
    <AppContainer>
      <Global styles={globalCSS} />
      <Nav>
        <Switch>
          {routes.map(route =>
            route.requiresAuthentication ? (
              <PrivateRoute
                key={route.path}
                path={route.path}
                exact={route.exact}
                component={route.component}
              />
            ) : (
              <Route
                key={route.path}
                exact={route.exact}
                path={route.path}
                component={route.component}
              />
            )
          )}
          <Route component={NotFound} />
        </Switch>
      </Nav>
    </AppContainer>
  </ThemeProvider>
)

export default Root
