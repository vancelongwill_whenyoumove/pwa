import styled from "@emotion/styled"

interface IDrawerProps {
  isOpen: boolean
}

export const Drawer = styled.div<IDrawerProps>`
  position: absolute;
  left: 0;
  top: 0;
  background: rgba(255, 255, 255, 0.9);
  overflow: hidden;
  width: 80vw;
  min-height: 100vh;
  height: 100%;
  transform: translateX(${props => (props.isOpen ? "0" : "-80vw")});
  transition: transform 0.3s ease-out;
  z-index: 4;
`
