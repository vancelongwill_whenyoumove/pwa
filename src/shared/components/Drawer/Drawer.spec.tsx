import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { Drawer } from "./Drawer"

describe("Drawer", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<Drawer isOpen />)).toMatchSnapshot()
  })
})
