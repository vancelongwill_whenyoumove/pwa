import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { CaptureImage } from "./CaptureImage"

import mockNavigator from "../../../__mocks__/navigatorMock"

describe("CaptureImage", () => {
  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ;(global as any).navigator.mediaDevices = mockNavigator().mediaDevices
  })
  it("should render without errors", () => {
    expect(
      mountWithTheme(<CaptureImage onImageCapture={jest.fn()} />)
    ).toMatchSnapshot()
  })
})
