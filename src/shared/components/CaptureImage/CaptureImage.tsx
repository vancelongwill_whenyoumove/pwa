import * as React from "react"

import Button from "../Button"

function hasCameraSupport(): boolean {
  return "mediaDevices" in navigator
}

export interface IProps {
  onImageCapture: (blob: Blob | null) => void
}
export const CaptureImage: React.FunctionComponent<IProps> = ({
  onImageCapture
}) => {
  const [showCapture, setShowCapture] = React.useState(false)
  const [imageCaptured, setImageCaptured] = React.useState(false)

  const videoEl = React.useRef<HTMLVideoElement>(null)
  // NB: could use document.createElement instead if the canvas preview will not be shown
  const canvasEl = React.useRef<HTMLCanvasElement>(null)

  const onCapture = React.useCallback(() => {
    if (canvasEl.current === null) {
      // tslint:disable-next-line: no-console
      console.log("Ref not attached to canvas element!")
      return
    }
    const context = canvasEl.current.getContext("2d")
    if (context === null) {
      // tslint:disable-next-line: no-console
      console.log("Unable to get canvas context")
      return
    }

    if (videoEl.current === null) {
      // tslint:disable-next-line: no-console
      console.log("Ref not attached to video element!")
      return
    }

    context.drawImage(
      videoEl.current,
      0,
      0,
      canvasEl.current.width,
      canvasEl.current.height
    )
    ;(videoEl.current.srcObject as MediaStream)
      .getVideoTracks()
      .forEach(track => track.stop())

    setImageCaptured(true)
    canvasEl.current.toBlob(onImageCapture)
  }, [onImageCapture])

  const onRecapture = React.useCallback(() => {
    setImageCaptured(imageCaptured => (imageCaptured ? false : imageCaptured))
  }, [])

  React.useEffect(() => {
    const currentVideoEl = videoEl.current
    if (!imageCaptured) {
      navigator.mediaDevices
        .getUserMedia({ video: true, audio: false })
        .then(stream => {
          if (currentVideoEl === null) {
            // tslint:disable-next-line: no-console
            console.log("Ref not attached to video element!")
            return
          }
          currentVideoEl.srcObject = stream
        })
    }
    if (videoEl.current !== null) {
      return () => {
        if (!currentVideoEl) {
          return
        }
        ;(currentVideoEl.srcObject as MediaStream)
          .getVideoTracks()
          .forEach(track => track.stop())
      }
    }
  }, [imageCaptured])

  if (!hasCameraSupport) {
    return (
      <p>
        Image capture support is not available, please use a supported browser
      </p>
    )
  }

  if (!showCapture) {
    return (
      <Button onClick={() => setShowCapture(true)}>Take a new photo</Button>
    )
  }

  return (
    <div>
      <video
        style={{ display: imageCaptured ? "none" : "inherit" }}
        ref={videoEl}
        controls={false}
        autoPlay
      />
      <canvas
        style={{ display: "none" }}
        ref={canvasEl}
        width="320"
        height="240"
      />
      <Button onClick={imageCaptured ? onRecapture : onCapture}>
        {imageCaptured ? "Take another photo" : "Take a photo"}
      </Button>
    </div>
  )
}
