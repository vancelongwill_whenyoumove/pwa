import * as React from "react"

import { act } from "react-dom/test-utils"

import { mountWithTheme } from "../../lib/testUtils"

import { PhotoPreview } from "./PhotoPreview"

describe("PhotoPreview", () => {
  it("should render without errors", () => {
    const file = new File([], "somefile")
    expect(
      act(() => {
        mountWithTheme(<PhotoPreview file={file} />)
      })
    ).toMatchSnapshot()
  })
})
