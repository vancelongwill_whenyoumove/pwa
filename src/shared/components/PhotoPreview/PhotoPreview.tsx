import * as React from "react"

import { useFileLoad } from "../../modules/files/hooks"

import Image from "../Image"

export const PreviewPDF: React.FunctionComponent<{ src: string }> = ({
  src
}) => (
  <object data={src} type="application/pdf">
    <embed src={src} type="application/pdf" />
  </object>
)

export interface IProps {
  file: File
}

export const PhotoPreview: React.FunctionComponent<IProps> = ({ file }) => {
  const state = useFileLoad(file)

  return file.type !== "application/pdf" ? (
    <Image aspectRatio={1} src={state.loading ? "" : state.data} />
  ) : (
    <PreviewPDF src={state.data} />
  )
}
