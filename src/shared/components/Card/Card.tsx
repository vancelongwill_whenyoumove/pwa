import * as React from "react"

import styled from "@emotion/styled"

import Image from "../Image"

export const CardFooter = styled.div`
  white-space: pre-line;
`
export const CardHeader = styled.div``
export const CardBody = styled.div``
export const CardContent = styled.div``

enum Border {
  LEFT,
  TOP,
  RIGHT
}

export function getBorderRadius({ border }: { border: Border }): string {
  switch (border) {
    case Border.RIGHT:
      return "0 0.5rem 0.5rem 0"
    case Border.LEFT:
      return "0.5rem 0 0 0.5rem"
    case Border.TOP:
    default:
      return "0.5rem 0.5rem 0 0"
  }
}

interface ICardImageProps {
  height?: string
  width?: string
  border: Border
}

export const CardImageContainer = styled.div<ICardImageProps>`
  border-radius: ${getBorderRadius};
  display: flex;
  min-height: ${p => p.height || "auto"};
  min-width: ${p => p.width || "auto"};

  & img,
  & div {
    border-radius: ${getBorderRadius};
  }
`

type Tail = "left" | "right"

interface ICardProps {
  image?: string | JSX.Element
  imageHeight?: string
  imageWidth?: string
  horizontal?: boolean
  alignRight?: boolean
  tail?: Tail
}

export function getTailStyles({ tail }: { tail?: Tail }): string {
  switch (tail) {
    case "left":
      return `
          width: 80vw;
          justify-self: flex-start;
          justify-content: flex-end;
          transform: translateX(calc(-10vw - 1rem));
        `
    case "right":
      return `
          width: 80vw;
          justify-self: flex-start;
          transform: translateX(calc(10vw + 1rem));
        `
    default:
      return ""
  }
}

export const CardContainer = styled.section<ICardProps>`
  display: flex;
  flex-direction: ${p => (p.horizontal ? "row" : "column")};
  color: ${p => p.theme.colors.black};
  background-color: ${p => p.theme.colors.white};
  border-radius: 0.5rem;
  box-shadow: 0 1px 4px grey;
  text-align: left;
  width: 100%;
  ${getTailStyles}
`

export const CardMain = styled.div<ICardProps>`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 1rem;

  /* stylelint-disable */
  ${CardHeader},
  ${CardBody},
  ${CardFooter} {
    color: ${p => p.theme.colors.black};
    text-align: ${props => (props.alignRight ? "right" : "inherit")};
  }
  /* stylelint-enable */

  ${CardHeader} {
    font-size: 1.2rem;
  }

  ${CardBody} {
    font-size: 1rem;
  }

  ${CardFooter} {
    color: ${p => p.theme.colors.darkGrey};
    font-size: 0.8rem;
  }
`

export class Card extends React.Component<ICardProps> {
  public static Header = CardHeader
  public static Body = CardBody
  public static Footer = CardFooter

  public render(): React.ReactNode {
    const { children, tail, image, horizontal, alignRight } = this.props
    return (
      <CardContainer tail={tail} horizontal={horizontal}>
        {image && !alignRight && this.Image()}
        <CardMain alignRight={alignRight} horizontal={horizontal}>
          {children}
        </CardMain>
        {image && alignRight && this.Image()}
      </CardContainer>
    )
  }

  private Image(): React.ReactNode {
    const { image, imageHeight, imageWidth } = this.props
    return (
      <CardImageContainer
        height={imageHeight}
        width={imageWidth}
        border={this.getImageBorder()}
      >
        {typeof image === "string" ? <Image src={image} lazy /> : image}
      </CardImageContainer>
    )
  }

  private getImageBorder(): Border {
    const { horizontal, alignRight } = this.props
    if (horizontal) {
      return alignRight ? Border.RIGHT : Border.LEFT
    } else {
      return Border.TOP
    }
  }
}
