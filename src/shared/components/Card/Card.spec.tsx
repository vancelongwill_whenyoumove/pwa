import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { Card } from "./Card"

describe("Card", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<Card />)).toMatchSnapshot()
  })
})
