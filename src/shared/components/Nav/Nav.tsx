import * as React from "react"
import { NavLink } from "react-router-dom"

import styled from "@emotion/styled"

import { colors } from "../../config/theme"

import routes from "../../routes"

import Drawer from "../Drawer"
import MenuBars from "../MenuBars"

const SidebarItem = styled.li`
  list-style: none;
  font-size: 2rem;
  padding: 1rem;
`

export const Sidebar: React.FunctionComponent<{
  onItemClick?: (routePath: string) => void
}> = ({ onItemClick }) => {
  const navItems = routes
    .filter(route => route.displayName)
    .map(route => (
      <SidebarItem
        onClick={() => onItemClick && onItemClick(route.path)}
        key={route.path}
      >
        <NavLink
          exact={route.exact}
          activeStyle={{ color: colors.darkBlue }}
          style={{ color: colors.darkGrey }}
          to={route.path}
        >
          {route.displayName}
        </NavLink>
      </SidebarItem>
    ))
  return <ul>{navItems}</ul>
}

export const PageLayout = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0;
`

export const Nav: React.FunctionComponent = ({ children }) => {
  const [isOpen, setOpen] = React.useState(false)

  const handleDrawerOpen = React.useCallback(() => {
    setOpen(true)
  }, [])

  const handleDrawerClose = React.useCallback(() => {
    setOpen(false)
  }, [])

  return (
    <>
      <MenuBars onClick={handleDrawerOpen} />
      <Drawer isOpen={isOpen}>
        <Sidebar onItemClick={handleDrawerClose} />
      </Drawer>
      <PageLayout onClick={handleDrawerClose}>{children}</PageLayout>
    </>
  )
}
