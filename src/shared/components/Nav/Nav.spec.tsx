import * as React from "react"

import { StaticRouter } from "react-router"

import { mountWithTheme } from "../../lib/testUtils"

import { Nav } from "./Nav"

describe("Nav component", () => {
  it("should match the snapshot", () => {
    expect(
      mountWithTheme(
        <StaticRouter>
          <Nav />
        </StaticRouter>
      )
    ).toMatchSnapshot()
  })
})
