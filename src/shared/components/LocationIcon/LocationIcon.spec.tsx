import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { LocationIcon } from "./LocationIcon"

describe("LocationIcon", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<LocationIcon />)).toMatchSnapshot()
  })
})
