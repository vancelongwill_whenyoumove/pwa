import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { PageTitle } from "./PageTitle"

describe("PageTitle", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<PageTitle />)).toMatchSnapshot()
  })
})
