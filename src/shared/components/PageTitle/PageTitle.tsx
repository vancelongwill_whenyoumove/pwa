import styled from "@emotion/styled"

export const PageTitle = styled.h1`
  padding-top: 15px;
  margin-top: 0;
  margin-left: 60px;
  vertical-align: center;
  align-self: flex-start;
  font-size: 1.5rem;
`
