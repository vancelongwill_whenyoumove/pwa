import * as React from "react"

import serializer from "jest-emotion"

import { mountWithTheme } from "../../lib/testUtils"

import { Dog } from "./Dog"

expect.addSnapshotSerializer(serializer)

describe("Dog component", () => {
  it("should match the snapshot", () => {
    expect(
      mountWithTheme(<Dog url="some-url-photo600x400" />)
    ).toMatchSnapshot()
  })
})
