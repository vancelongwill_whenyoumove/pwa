import * as React from "react"

import Image from "../Image"

export const Dog: React.FunctionComponent<{ url: string }> = props => {
  return <Image src={props.url} lazy />
}
