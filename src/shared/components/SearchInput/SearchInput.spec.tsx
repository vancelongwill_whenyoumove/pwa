import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { SearchInput } from "./SearchInput"

describe("SearchInput", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<SearchInput />)).toMatchSnapshot()
  })
})
