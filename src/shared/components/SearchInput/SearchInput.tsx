import styled from "@emotion/styled"

import Input from "../Input"

import SearchIconSVG from "../../../client/assets/images/icon-search.svg"

export const SearchInput = styled(Input)`
  border-radius: 2rem;
  height: 3rem;
  padding-left: 0.5rem;
  box-shadow: ${p => p.theme.defaultShadow};
`

SearchInput.defaultProps = {
  type: "search",
  icon: SearchIconSVG
}
