import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { Image } from "./Image"

describe("Image", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<Image />)).toMatchSnapshot()
  })
})
