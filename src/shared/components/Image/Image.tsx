import * as React from "react"

import styled from "@emotion/styled"
import { PropsOf } from "@emotion/styled-base/types/helper"

import execEnv from "../../lib/execEnv"

import errorSVG from "../../../client/assets/images/imgerror.svg"
import spinnerSVG from "../../../client/assets/images/spinner.svg"

const ImageContainer = styled.div<{ aspectRatio: number; error?: boolean }>`
  width: 100%;
  height: 0;
  margin: 0;
  padding-bottom: ${p => p.aspectRatio * 100}%;
  position: relative;
  background-color: ${p => p.theme.colors.white};
  background-image: url(${p => (p.error ? errorSVG : spinnerSVG)});
  background-position: center;
  background-repeat: no-repeat;
  background-size: ${p => (p.error ? "contain" : "auto")};
`

const Img = styled.img<{ error?: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: auto;
  display: ${p => (p.error ? "none" : "block")};
`

export interface IProps extends PropsOf<typeof Img> {
  aspectRatio?: number
  lazy?: boolean
  onLoad?: never
  onError?: never
  ref?: never
}

const dimensionsRegex = new RegExp("([0-9]+)x([0-9]+)")
export function getAspectRatioFromURL(url: string): number {
  const dimensions = dimensionsRegex.exec(url)
  if (!dimensions || dimensions.length < 2) {
    throw new Error(`No dimensions found in url: ${url}`)
  }
  if (dimensions.length > 3) {
    throw new Error(`More than one set of dimensions found in url: ${url}`)
  }
  return Number(dimensions[2]) / Number(dimensions[1])
}

let lazyImageObserver: IntersectionObserver
if (!execEnv.isServer && execEnv.isInteractionObserverSupported) {
  lazyImageObserver = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        const lazyImage = entry.target as HTMLImageElement
        lazyImage.src = lazyImage.dataset.src || ""
        lazyImage.srcset = lazyImage.dataset.srcset || ""
        lazyImageObserver.unobserve(lazyImage)
      }
    })
  })
}

export const Image: React.FunctionComponent<IProps> = ({
  aspectRatio,
  src,
  srcSet,
  lazy,
  ...imgProps
}) => {
  const [hasError, setError] = React.useState(false)
  const imageRef = React.useRef<HTMLImageElement>()
  const [aspectCorrection, setAspectCorrection] = React.useState(0)

  let aspect = 1
  if (!aspectRatio && src) {
    try {
      aspect = getAspectRatioFromURL(src)
    } catch (e) {
      // tslint:disable-next-line: no-console
      console.warn("No aspect ratio provided for image: ", e)
    }
  }

  const finalAspect = aspectCorrection || aspectRatio || aspect

  const onRef = React.useCallback(
    (imageElement: HTMLImageElement | null) => {
      if (imageElement) {
        imageRef.current = imageElement

        if (lazy && lazyImageObserver) {
          lazyImageObserver.observe(imageElement)
        }
      }
    },
    [lazy]
  )

  const handleLoad = React.useCallback(() => {
    if (imageRef.current) {
      const currentAspect = imageRef.current.height / imageRef.current.width
      if (currentAspect !== finalAspect) {
        setAspectCorrection(currentAspect)
      }
    }
  }, [finalAspect])

  const handleError = React.useCallback(() => {
    if (src) {
      setError(true)
    }
  }, [src])

  const lazyProps = lazy
    ? {
        "data-src": src,
        "data-srcset": srcSet
      }
    : {
        src,
        srcSet
      }

  return (
    <ImageContainer aspectRatio={finalAspect} error={hasError}>
      <Img
        {...imgProps}
        {...lazyProps}
        onLoad={handleLoad}
        onError={handleError}
        error={hasError}
        ref={onRef}
      />
      <noscript>
        <Img {...imgProps} src={src} srcSet={srcSet} />
      </noscript>
    </ImageContainer>
  )
}
