import * as React from "react"

import styled from "@emotion/styled"
import { PropsOf } from "@emotion/styled-base/types/helper"

const StyledInput = styled.input`
  box-shadow: none;
  border: none;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  padding-left: 0.5rem;
  font-size: 1rem;
  width: 100%;
  margin: 0;
  background: transparent;
`

interface IWrapperProps {
  border?: boolean
}

const InputWrapper = styled.div<{ border?: boolean }>`
  border-radius: 2px;
  width: 100%;
  display: flex;
  border-bottom-style: solid;
  border-bottom-color: ${p => p.theme.colors.darkBlue};
  border-bottom-width: ${p => (p.border ? "1px" : "0")};
  background-color: ${p => p.theme.colors.white};
`

interface IProps extends PropsOf<typeof StyledInput>, IWrapperProps {
  icon?: React.ReactNode | string
}

const StyledIcon = styled.span<{ iconSrc: string }>`
  padding: 0.5rem;
  padding-left: 1rem;
  padding-right: 1rem;
  max-width: 1rem;
  margin: 0;
  position: relative;

  &::before {
    position: absolute;
    text-align: center;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    content: "";
    height: 100%;
    width: 60%;
    background-image: url(${p => p.iconSrc});
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
    vertical-align: middle;
  }
`

StyledIcon.defaultProps = {
  role: "img"
}

export const Input: React.FunctionComponent<IProps> = ({
  border,
  icon,
  className,
  ...rest
}) => {
  let iconComponent
  if (icon) {
    iconComponent =
      typeof icon === "string" ? <StyledIcon iconSrc={icon} /> : icon
  }

  return (
    <InputWrapper className={className} border={border}>
      {icon && iconComponent}
      <StyledInput {...rest} />
    </InputWrapper>
  )
}
