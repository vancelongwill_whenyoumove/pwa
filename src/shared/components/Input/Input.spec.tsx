import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { Input } from "./Input"

describe("Input", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<Input />)).toMatchSnapshot()
  })
})
