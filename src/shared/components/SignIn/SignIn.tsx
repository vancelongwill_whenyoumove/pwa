import * as React from "react"

import styled from "@emotion/styled"
import { colors } from "../../config/theme"

import Button from "../Button"
import Input from "../Input"

import EmailIconSVG from "../../../client/assets/images/icon-email.svg"
import PasswordIconSVG from "../../../client/assets/images/icon-password.svg"

interface IProps {
  onSubmit: ({ user, password }: { user: string; password: string }) => void
}

const SignInContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  width: 90%;

  * {
    margin: 1rem;
  }
`

export const SignIn: React.FunctionComponent<IProps> = ({ onSubmit }) => {
  const [user, setUser] = React.useState("")
  const [password, setPassword] = React.useState("")

  const handleUserChange = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setUser(e.target.value)
    },
    []
  )

  const handlePasswordChange = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setPassword(e.target.value)
    },
    []
  )

  const handleSubmit = React.useCallback(() => {
    onSubmit({ user, password })
  }, [user, password, onSubmit])

  return (
    <SignInContainer>
      <Input
        type="email"
        placeholder="user@example.com"
        value={user}
        onChange={handleUserChange}
        icon={EmailIconSVG}
        border
      />
      <Input
        type="password"
        placeholder="password"
        value={password}
        onChange={handlePasswordChange}
        icon={PasswordIconSVG}
        border
      />
      <Button color={colors.lightBlue} onClick={handleSubmit}>
        Submit
      </Button>
    </SignInContainer>
  )
}
