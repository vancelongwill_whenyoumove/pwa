import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { SignIn } from "./SignIn"

describe("SignIn component", () => {
  it("should match the snapshot", () => {
    expect(mountWithTheme(<SignIn onSubmit={jest.fn()} />)).toMatchSnapshot()
  })
})
