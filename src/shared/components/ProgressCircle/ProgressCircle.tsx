import * as React from "react"

import styled from "@emotion/styled"

const Path = styled.path<{
  percentage: number
}>`
  stroke-dasharray: 251.2, 251.2;
  stroke-dashoffset: ${p => ((100 - p.percentage) / 100) * 251.2};
  stroke-linecap: round;
  stroke-width: 5;
  fill: none;
  transition: stroke-dasharray 0.5s linear;
`

const Svg = styled.svg`
  width: 100%;
`

interface IProps {
  strokeColor: string
  backgroundColor: string
  textColor: string
  percentage: number
}
export const ProgressCircle: React.FunctionComponent<IProps> = ({
  strokeColor,
  backgroundColor,
  textColor,
  percentage
}) => {
  return (
    <Svg id="animated" viewBox="0 0 100 100">
      <circle cx="50" cy="50" r="45" fill={backgroundColor} />
      <Path
        percentage={100}
        stroke="rgba(0, 0, 0, 0.2)"
        d="M50 10
           a 40 40 0 0 1 0 80
           a 40 40 0 0 1 0 -80"
      />
      <Path
        percentage={percentage}
        stroke={strokeColor}
        d="M50 10
           a 40 40 0 0 1 0 80
           a 40 40 0 0 1 0 -80"
      />
      <text
        fill={textColor}
        x="50"
        y="50"
        textAnchor="middle"
        dy="10"
        fontSize="32"
      >
        {percentage}%
      </text>
    </Svg>
  )
}
