import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { ProgressCircle } from "./ProgressCircle"

describe("ProgressCircle", () => {
  it("should render without errors", () => {
    expect(
      mountWithTheme(
        <ProgressCircle
          strokeColor="blue"
          backgroundColor="red"
          textColor="grey"
          percentage={60}
        />
      )
    ).toMatchSnapshot()
  })
})
