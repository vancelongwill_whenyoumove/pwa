import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { Flex } from "./Flex"

describe("Flex", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<Flex />)).toMatchSnapshot()
  })
})
