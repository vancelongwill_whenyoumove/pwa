import styled from "@emotion/styled"

export const Flex = styled.div<{ column?: boolean }>`
  flex-direction: ${p => (p.column ? "column" : "row")};
  align-items: center;
  justify-content: center;
  width: 100%;
`
