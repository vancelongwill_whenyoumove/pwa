import * as React from "react"

import styled from "@emotion/styled"
import { PropsOf } from "@emotion/styled-base/types/helper"

import { setOpacity } from "../../config/theme"

interface IProps {
  alwaysShow?: boolean
}

export const Container = styled.div<IProps>`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 5px;
  height: 100%;
  width: 100%;
  transition: 0.5s ease;
  background-color: ${p => setOpacity(p.theme.colors.black, 0.4)};
  opacity: ${props => (props.alwaysShow ? 1 : 0)};
  font-size: 20;

  &:hover {
    opacity: 1;
  }
`

export const Bottom = styled.div`
  position: absolute;
  bottom: 0;
  text-align: center;
  left: 50%;
  transform: translateX(-50%);
  color: ${p => p.theme.colors.lightGrey};
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`

export const Center = styled.div`
  color: ${p => p.theme.colors.white};
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 90%;
  text-align: center;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`

export const Top = styled.div`
  position: absolute;
  top: 0;
  width: 80%;
  text-align: center;
  left: 50%;
  transform: translate(-50%);
  color: ${p => p.theme.colors.lightGrey};
`

export class Overlay extends React.Component<PropsOf<typeof Container>> {
  public static Bottom = Bottom
  public static Center = Center
  public static Top = Top
  public render(): React.ReactNode {
    return <Container {...this.props} />
  }
}
