import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { Overlay } from "./Overlay"

describe("Overlay", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<Overlay />)).toMatchSnapshot()
  })
})
