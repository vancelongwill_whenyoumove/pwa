import * as React from "react"

import styled from "@emotion/styled"

export const StyledButton = styled.button<{
  stretch?: boolean
  color?: string
}>`
  min-width: ${p => (p.stretch ? "100%" : "8rem")};
  background-color: ${p => (p.color ? p.color : p.theme.colors.lightBlue)};
  height: 3rem;
  margin: 1rem;
  border-radius: 2rem;
  border: none;
  font-size: 1.2rem;
  color: ${p => p.theme.colors.white};
  font-weight: 700;
`

interface IBaseButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  className?: string
}

interface IButtonPropsWithValue<T = undefined>
  extends Omit<IBaseButtonProps, "onClick" | "value"> {
  onClick?: (value: T) => void
  value?: T
}

export function Button<T = undefined>({
  onClick,
  value,
  ...rest
}: T extends undefined
  ? IBaseButtonProps
  : IButtonPropsWithValue<T>): ReturnType<
  React.FunctionComponent<IButtonPropsWithValue<T>>
> {
  const handleClick = React.useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      if (onClick) {
        if (typeof value === "undefined") {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          onClick(e as any)
        } else {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          onClick(value as any)
        }
      }
    },
    [value, onClick]
  )
  return <StyledButton onClick={handleClick} {...rest} />
}
