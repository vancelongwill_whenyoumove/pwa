import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { Button } from "./Button"

describe.only("Button", () => {
  it("should render without errors", () => {
    expect(mountWithTheme(<Button>Some button</Button>)).toMatchSnapshot()
  })
})
