import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { FileUpload } from "./FileUpload"

describe("FileUpload", () => {
  it("should render without errors", () => {
    const file = new File([], "somefile.txt")
    expect(
      mountWithTheme(<FileUpload file={file} endpoint="api/something" />)
    ).toMatchSnapshot()
  })
})
