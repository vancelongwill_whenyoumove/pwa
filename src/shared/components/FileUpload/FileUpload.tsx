import * as React from "react"

import useFileUpload from "../../modules/uploads/hooks"

import Button from "../Button"

export interface IProps {
  file: File
  endpoint: string
  onSuccess?: (file: File) => void
  onFail?: (file: File, e: Error) => void
}

export const FileUpload: React.FunctionComponent<IProps> = ({
  file,
  endpoint,
  onSuccess,
  onFail
}) => {
  const state = useFileUpload({ file, endpoint, onSuccess, onFail })
  return (
    <div>
      <progress max="100" value={state.progress} />
      {state.loading && (
        <Button onClick={() => state.source.cancel()}>Cancel</Button>
      )}
      {state.error && !state.loading && (
        <div>
          Could not upload this file, click to retry
          <Button onClick={() => console.log("retry")} />
        </div>
      )}
      {state.done && <p>Done.</p>}
    </div>
  )
}
