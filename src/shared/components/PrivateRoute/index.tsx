import { connect } from "react-redux"
import { IState } from "../../store/types"
import { PrivateRoute } from "./PrivateRoute"

export function mapStateToProps(state: IState): { isAuthenticated: boolean } {
  return {
    isAuthenticated: state.auth.isAuthenticated
  }
}

export default connect(mapStateToProps)(PrivateRoute)
