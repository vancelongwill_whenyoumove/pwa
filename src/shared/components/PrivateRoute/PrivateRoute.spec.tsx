import * as React from "react"

import { StaticRouter } from "react-router"

import { mountWithTheme } from "../../lib/testUtils"

import { PrivateRoute } from "./PrivateRoute"

describe("PrivateRoute", () => {
  it("should render without errors", () => {
    expect(
      mountWithTheme(
        <StaticRouter>
          <PrivateRoute
            component={jest.fn().mockImplementation(() => (
              <div />
            ))}
            isAuthenticated
            path="/"
          />
        </StaticRouter>
      )
    ).toMatchSnapshot()
  })
})
