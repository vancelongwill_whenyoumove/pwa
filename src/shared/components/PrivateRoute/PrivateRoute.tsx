import * as React from "react"
import { Redirect, Route } from "react-router-dom"

export interface IProps {
  component: ({ ...props }: object) => React.ReactElement
  isAuthenticated: boolean
  // ...rest
  [x: string]: unknown
}
export const PrivateRoute: React.FunctionComponent<IProps> = ({
  component: Component,
  isAuthenticated,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/signin",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  )
}
