import * as React from "react"

import styled from "@emotion/styled"

import execEnv from "../../lib/execEnv"

const FilePickerContainer = styled.div<{
  isDragging: boolean
}>`
  height: 100%;
  width: 100%;
  min-height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  /* stylelint-disable declaration-colon-newline-after */
  background-color: ${p =>
    p.isDragging ? p.theme.colors.darkblue : p.theme.colors.lightBlue};
  /* stylelint-enable */
  border-radius: 5px;
  border-width: 5px;
  border-style: dashed;
  /* stylelint-disable declaration-colon-newline-after */
  border-color: ${p =>
    p.isDragging ? p.theme.colors.black : p.theme.colors.grey};
  /* stylelint-enable */
`

const FilePickerLabel = styled.label`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  color: ${p => p.theme.colors.white};
  width: 100%;
  height: 100%;
`

const FilePickerInput = styled.input<{ isLegacy: boolean }>`
  padding: 20px;
  display: ${props => (props.isLegacy ? "inherit" : "none")};
`

export interface IProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  promptText: string
  onFilesChange(files: FileList): void
}
export interface IState {
  isLegacy: boolean
  isDragging: boolean
}

export class FilePicker extends React.Component<IProps, IState> {
  public state: IState = {
    isDragging: false,
    isLegacy: false
  }

  public componentDidMount(): void {
    if (!execEnv.isDraggingSupported) {
      this.setState(state => ({ ...state, isLegacy: true }))
    }
  }

  public handleDrop = (e: React.DragEvent<HTMLDivElement>) => {
    e.stopPropagation()
    e.preventDefault()

    this.setState(state => ({ ...state, isDragging: false }))

    this.props.onFilesChange(e.dataTransfer.files)
  }

  public handleDragEnter = (e: React.DragEvent<HTMLDivElement>) => {
    e.stopPropagation()
    e.preventDefault()
    e.dataTransfer.dropEffect = "copy"

    if (!this.state.isDragging) {
      this.setState(state => ({ ...state, isDragging: true }))
    }
  }
  public handleDragLeave = (e: React.DragEvent<HTMLDivElement>) => {
    e.stopPropagation()
    e.preventDefault()

    this.setState(state => ({ ...state, isDragging: false }))
  }

  public render(): React.ReactNode {
    const { isDragging, isLegacy } = this.state
    const { onFilesChange, promptText, ...inputProps } = this.props
    return (
      <FilePickerContainer
        isDragging={isDragging}
        onDrop={this.handleDrop}
        onDragOver={this.handleDragEnter}
        onDragEnter={this.handleDragEnter}
        onDragLeave={this.handleDragLeave}
      >
        <FilePickerLabel htmlFor="filePickerInput">
          <span>{promptText}</span>
        </FilePickerLabel>
        <FilePickerInput
          isLegacy={isLegacy}
          type="file"
          capture="user"
          onChange={e => e.target.files && onFilesChange(e.target.files)}
          {...inputProps}
        />
      </FilePickerContainer>
    )
  }
}
