import * as React from "react"

import { mountWithTheme } from "../../lib/testUtils"

import { FilePicker } from "./FilePicker"

describe("FilePicker", () => {
  it("should render without errors", () => {
    expect(
      mountWithTheme(
        <FilePicker promptText="Add files" onFilesChange={jest.fn()} />
      )
    ).toMatchSnapshot()
  })
})
