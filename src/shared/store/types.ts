import { IState as IAuthState } from "../modules/auth"
import { IState as IDogsState } from "../modules/dogs"
import { IState as IPropertiesState } from "../modules/properties"

export interface IState {
  dogs: IDogsState
  auth: IAuthState
  properties: IPropertiesState
}
