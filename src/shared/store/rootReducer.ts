import { combineReducers } from "redux"

import { reducer as auth } from "../modules/auth"
import { reducer as dogs } from "../modules/dogs"
import { reducer as properties } from "../modules/properties"

export default combineReducers({
  auth,
  dogs,
  properties
  // add reducers here
})
