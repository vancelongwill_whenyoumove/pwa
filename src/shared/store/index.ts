import { applyMiddleware, compose, createStore, Store } from "redux"
import createSagaMiddleware, { END } from "redux-saga"

import { Persistor, persistStore } from "redux-persist"
// import storage from "redux-persist/lib/storage" // defaults to localStorage for web

import execEnv from "../lib/execEnv"

import rootReducer from "./rootReducer"
import rootSaga from "./rootSaga"

import { DeepPartial } from "ts-essentials"
import { IState } from "./types"

import { NODE_ENV } from "../config/env"

export interface IStoreWithSaga extends Store {
  runSaga: () => { toPromise: () => Promise<never> }
  closeSaga: () => void
}

export function makeStore(
  preloadedState: DeepPartial<IState> = {}
): { store: IStoreWithSaga; persistor: Persistor } {
  const sagaMiddleware = createSagaMiddleware()

  let composeEnhancers = compose

  // tslint:disable-next-line strict-type-predicates
  if (!execEnv.isServer) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  }

  const store = createStore(
    rootReducer,
    preloadedState,
    composeEnhancers(applyMiddleware(sagaMiddleware))
  ) as IStoreWithSaga

  const persistor = persistStore(store)

  if (execEnv.isServer) {
    // Attach run & close functions to store so that sagas can be run on each request
    store.runSaga = () => sagaMiddleware.run(rootSaga)
    store.closeSaga = () => store.dispatch(END)
  } else {
    sagaMiddleware.run(rootSaga)
  }

  // HMR
  if (NODE_ENV !== "production") {
    if (module.hot) {
      module.hot.accept("./rootReducer", () =>
        store.replaceReducer(require("./rootReducer").default)
      )
    }
  }

  return { store, persistor }
}
