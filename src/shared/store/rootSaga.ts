/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { all, fork } from "redux-saga/effects"

import { saga as authSaga } from "../modules/auth"
import { saga as dogsSaga } from "../modules/dogs"
import { saga as propertiesSaga } from "../modules/properties"

function* rootSaga() {
  yield all([
    fork(dogsSaga),
    fork(authSaga),
    fork(propertiesSaga)
    // add sagas here
  ])
}

export default rootSaga
