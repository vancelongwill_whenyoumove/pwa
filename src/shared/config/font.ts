import { css } from "@emotion/core"

import sfProBlack from "../../client/assets/font/SF-Pro-Display-Black.otf"
import sfProBlackItalic from "../../client/assets/font/SF-Pro-Display-BlackItalic.otf"
import sfProBold from "../../client/assets/font/SF-Pro-Display-Bold.otf"
import sfProBoldItalic from "../../client/assets/font/SF-Pro-Display-BoldItalic.otf"
import sfProHeavy from "../../client/assets/font/SF-Pro-Display-Heavy.otf"
import sfProHeavyItalic from "../../client/assets/font/SF-Pro-Display-HeavyItalic.otf"
import sfProLight from "../../client/assets/font/SF-Pro-Display-Light.otf"
import sfProLightItalic from "../../client/assets/font/SF-Pro-Display-LightItalic.otf"
import sfProMedium from "../../client/assets/font/SF-Pro-Display-Medium.otf"
import sfProMediumItalic from "../../client/assets/font/SF-Pro-Display-MediumItalic.otf"
import sfProRegular from "../../client/assets/font/SF-Pro-Display-Regular.otf"
import sfProRegularItalic from "../../client/assets/font/SF-Pro-Display-RegularItalic.otf"
import sfProSemibold from "../../client/assets/font/SF-Pro-Display-Semibold.otf"
import sfProSemiboldItalic from "../../client/assets/font/SF-Pro-Display-SemiboldItalic.otf"
import sfProThin from "../../client/assets/font/SF-Pro-Display-Thin.otf"
import sfProThinItalic from "../../client/assets/font/SF-Pro-Display-ThinItalic.otf"
import sfProUltralight from "../../client/assets/font/SF-Pro-Display-Ultralight.otf"
import sfProUltralightItalic from "../../client/assets/font/SF-Pro-Display-UltralightItalic.otf"

export const font = {
  sfProBlack,
  sfProBlackItalic,
  sfProBold,
  sfProBoldItalic,
  sfProHeavy,
  sfProHeavyItalic,
  sfProLight,
  sfProLightItalic,
  sfProMedium,
  sfProMediumItalic,
  sfProRegular,
  sfProRegularItalic,
  sfProSemibold,
  sfProSemiboldItalic,
  sfProThin,
  sfProThinItalic,
  sfProUltralight,
  sfProUltralightItalic
}

export const fontFace = css`
  /** Black */
  @font-face {
    font-family: "SF Display";
    font-weight: 900;
    src: url(${font.sfProBlack});
  }

  /** Black Italic */
  @font-face {
    font-family: "SF Display";
    font-weight: 900;
    font-style: italic;
    src: url(${font.sfProBlackItalic});
  }

  /** Bold */
  @font-face {
    font-family: "SF Display";
    font-weight: 700;
    src: url(${font.sfProBold});
  }

  /** Bold Italic */
  @font-face {
    font-family: "SF Display";
    font-weight: 700;
    font-style: italic;
    src: url(${font.sfProBoldItalic});
  }

  /** Heavy */
  @font-face {
    font-family: "SF Display";
    font-weight: 800;
    src: url(${font.sfProHeavy});
  }

  /** Heavy Italic */
  @font-face {
    font-family: "SF Display";
    font-weight: 800;
    font-style: italic;
    src: url(${font.sfProHeavyItalic});
  }

  /** Light */
  @font-face {
    font-family: "SF Display";
    font-weight: 200;
    src: url(${font.sfProLight});
  }

  /** Light Italic */
  @font-face {
    font-family: "SF Display";
    font-weight: 200;
    font-style: italic;
    src: url(${font.sfProLightItalic});
  }

  /** Medium */
  @font-face {
    font-family: "SF Display";
    font-weight: 500;
    src: url(${font.sfProMedium});
  }

  /** Medium Italic */
  @font-face {
    font-family: "SF Display";
    font-weight: 500;
    font-style: italic;
    src: url(${font.sfProMediumItalic});
  }

  /** Regular */
  @font-face {
    font-family: "SF Display";
    font-weight: 400;
    src: url(${font.sfProRegular});
  }

  /** Regular Italic */
  @font-face {
    font-family: "SF Display";
    font-weight: 400;
    font-style: italic;
    src: url(${font.sfProRegularItalic});
  }

  /** Semibold */
  @font-face {
    font-family: "SF Display";
    font-weight: 600;
    src: url(${font.sfProSemibold});
  }

  /** Semibold Italic */
  @font-face {
    font-family: "SF Display";
    font-weight: 600;
    font-style: italic;
    src: url(${font.sfProSemiboldItalic});
  }

  /** Thin */
  @font-face {
    font-family: "SF Display";
    font-weight: 300;
    src: url(${font.sfProThin});
  }

  /** Thin Italic */
  @font-face {
    font-family: "SF Display";
    font-weight: 300;
    font-style: italic;
    src: url(${font.sfProThinItalic});
  }

  /** Ultralight */
  @font-face {
    font-family: "SF Display";
    font-weight: 100;
    src: url(${font.sfProUltralight});
  }

  /** Ultralight Italic */
  @font-face {
    font-family: "SF Display";
    font-weight: 100;
    font-style: italic;
    src: url(${font.sfProUltralightItalic});
  }
`
