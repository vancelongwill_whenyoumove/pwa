// ENVIRONMENT VARIABLES
export const WYM_API_ROOT = process.env.WYM_API_ROOT as string
export const NODE_ENV = process.env.NODE_ENV as string
export const PORT = process.env.PORT as string
export const DEBUG_WEBPACK = process.env.DEBUG_WEBPACK === "true"
export const GOOGLE_MAPS_API_KEY = process.env.GOOGLE_MAPS_API_KEY as string
