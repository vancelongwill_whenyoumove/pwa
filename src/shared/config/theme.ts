const black = "rgba(4, 27, 21, 1)"
const darkBlue = "rgba(42, 98, 143, 1)"
const lightBlue = "rgba(104, 195, 199, 1)"
const lightGrey = "rgba(235, 233, 233, 1)"
const darkGrey = "rgba(54, 54, 53, 1)"
const white = "rgba(255, 255, 250, 1)"

export interface IColors {
  black: typeof black
  darkBlue: typeof darkBlue
  darkGrey: typeof darkGrey
  lightBlue: typeof lightBlue
  lightGrey: typeof lightGrey
  white: typeof white
}

export interface ITheme {
  colors: IColors
  defaultShadow: string
}

export const colors: IColors = {
  black,
  darkBlue,
  darkGrey,
  lightBlue,
  lightGrey,
  white
}

const theme: ITheme = {
  colors,
  defaultShadow: `0 1px 4px ${darkGrey}`
}

type ValueOf<T> = T[keyof T]

export function setOpacity(color: ValueOf<IColors>, opacity: number): string {
  return color.replace("1)", `${opacity})`)
}

export default theme
