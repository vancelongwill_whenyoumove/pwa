// tslint:disable object-literal-sort-keys
import Account from "./views/Account"
import Dashboard from "./views/Dashboard"
import Home from "./views/Home"
import Map from "./views/Map"
import Photo from "./views/Photo"
import SignIn from "./views/SignIn"

const routes = [
  {
    displayName: "Home",
    path: "/",
    exact: true,
    component: Home
  },
  {
    displayName: "Sign In",
    path: "/signin",
    component: SignIn
  },
  {
    displayName: "My Account",
    path: "/my-account",
    component: Account,
    requiresAuthentication: true
  },
  {
    displayName: "Photo",
    path: "/photo",
    component: Photo
  },
  {
    displayName: "Map",
    path: "/map",
    component: Map
  },
  {
    displayName: "Dashboard",
    path: "/dashboard",
    component: Dashboard
  },
  {
    // This route will be used to provide our PWA app shell for cacheing all routes
    // 'displayName' is omitted so that it won't show up in our navigation
    path: "/app-shell",
    component: () => null // No component
  }
]

export default routes
