import axios, { AxiosPromise } from "axios"
import { WYM_API_ROOT } from "../config/env"

interface ISigninResponse {
  [x: string]: unknown
}

interface IRegisterResponse {
  [x: string]: unknown
}

class AuthService {
  public static signIn({
    user,
    password
  }: {
    user: string
    password: string
  }): AxiosPromise<ISigninResponse> {
    return axios.post<ISigninResponse>(`${WYM_API_ROOT}/signin`, {
      user,
      password
    })
  }
  public static register({
    user,
    password
  }: {
    user: string
    password: string
  }): AxiosPromise<IRegisterResponse> {
    return axios.post<IRegisterResponse>(`${WYM_API_ROOT}/register`, {
      user,
      password
    })
  }
}

export default AuthService
