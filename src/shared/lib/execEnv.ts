export function isDOMUsable(): boolean {
  return Boolean(
    typeof window !== "undefined" &&
      window.document &&
      window.document.createElement
  )
}
export function isServer(): boolean {
  return !isDOMUsable()
}

export function isGeolocationSupported(): boolean {
  return "geolocation" in navigator
}

export function isInteractionObserverSupported(): boolean {
  return "IntersectionObserver" in window
}

export function isDraggingSupported(): boolean {
  const div = document.createElement("div")
  return "draggable" in div || ("ondragstart" in div && "ondrop" in div)
}

export interface IExecutionEnvironment {
  isServer: boolean
  isGeolocationSupported: boolean
  isInteractionObserverSupported: boolean
  isDraggingSupported: boolean
}

export function getExecutionEnvironment(): IExecutionEnvironment {
  const server = isServer()
  const geolocation = !server && isGeolocationSupported()
  const interactionObserver = !server && isInteractionObserverSupported()
  const dragging = !server && isDraggingSupported()

  return {
    isServer: server,
    isGeolocationSupported: geolocation,
    isInteractionObserverSupported: interactionObserver,
    isDraggingSupported: dragging
  }
}

export default getExecutionEnvironment()
