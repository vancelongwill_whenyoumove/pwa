import styled, { CreateStyled } from "@emotion/styled"

import { ITheme } from "../config/theme"
// @TODO: Use the default export of this file instead of @emotion/styled once https://github.com/emotion-js/emotion/pull/1203 is merged
// This will allow a strongly typed theme prop
export default styled as CreateStyled<ITheme>
