import * as React from "react"

import { ThemeProvider } from "emotion-theming"
import { mount, ReactWrapper } from "enzyme"

import theme from "../config/theme"

export function mountWithTheme(jsx: JSX.Element): ReactWrapper {
  return mount(<ThemeProvider theme={theme}>{jsx}</ThemeProvider>)
}
