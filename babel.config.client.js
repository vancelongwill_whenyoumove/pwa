const presets = [
  "@babel/preset-react",
  [
    "@babel/env",
    {
      targets: {
        edge: "17",
        firefox: "60",
        chrome: "67",
        safari: "11.1",
        ie: "11"
      }
    }
  ],
  "@babel/preset-typescript"
]

const plugins = [
  "transform-class-properties",
  "@babel/plugin-transform-runtime"
]

module.exports = api => {
  const isProduction = api.env("production")
  api.cache(true)
  // You can use isProduction to determine what presets and plugins to use.
  if (!isProduction) {
    plugins.unshift(["emotion", { sourceMap: true, autoLabel: true }])
  } else {
    plugins.unshift(["emotion", { hoist: true }])
  }

  return { presets, plugins }
}
