/* eslint-disable @typescript-eslint/camelcase */
import * as MiniCssExtractPlugin from "mini-css-extract-plugin"
import * as path from "path"
import * as webpack from "webpack"
import * as merge from "webpack-merge"
import * as nodeExternals from "webpack-node-externals"

import { baseConfig } from "./webpack.development.config"

// config for using webpack-hot-server-middleware to reload the SSR part of the server app
const server: webpack.Configuration = merge(baseConfig, {
  name: "server",
  target: "node",
  node: {
    __dirname: false
  },
  entry: {
    ssr: "./src/server/ssr.tsx"
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    libraryTarget: "commonjs2"
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              configFile: "./babel.config.js"
            }
          }
        ],
        exclude: [/node_modules/]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              hmr: process.env.NODE_ENV === "development"
            }
          },
          "css-loader"
        ]
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path]-[name].[hash].[ext]",
              outputPath: "images"
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader"]
      }
    ]
  },
  externals: [nodeExternals()]
})

export default server
