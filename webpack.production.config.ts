/* eslint-disable @typescript-eslint/camelcase */
import * as Dotenv from "dotenv-webpack"
import * as HtmlWebpackPlugin from "html-webpack-plugin"
import * as MiniCssExtractPlugin from "mini-css-extract-plugin"
import * as path from "path"
import * as ScriptExtHtmlWebpackPlugin from "script-ext-html-webpack-plugin"
import * as webpack from "webpack"
import * as merge from "webpack-merge"
import * as nodeExternals from "webpack-node-externals"
import * as WebpackPwaManifest from "webpack-pwa-manifest"

import { InjectManifest } from "workbox-webpack-plugin"

import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer"

const baseConfig: webpack.Configuration = {
  mode: "production",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    modules: ["src", "node_modules"]
  },
  plugins: [
    new Dotenv(),
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("production")
    })
  ]
}

const client: webpack.Configuration = merge(baseConfig, {
  name: "client",
  target: "web",
  entry: [path.resolve(__dirname, "src", "client", "index.tsx")],
  output: {
    filename: "[name].[contenthash].min.js",
    path: path.resolve(__dirname, "public")
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              configFile: "./babel.config.client.js"
            }
          }
        ],
        exclude: [/node_modules/]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              hmr: process.env.NODE_ENV === "development"
            }
          },
          "css-loader"
        ]
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path]-[name].[contenthash].[ext]",
              outputPath: "images"
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader"]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].[contenthash].css",
      chunkFilename: "[id].[contenthash].css"
    }),
    new webpack.HashedModuleIdsPlugin(), // so that file hashes don't change unexpectedly
    new HtmlWebpackPlugin({
      title: "When You Move",
      template: path.resolve(
        __dirname,
        "src",
        "shared",
        "assets",
        "index.html"
      ),
      filename: path.resolve(__dirname, "public", "index.html")
    }),
    // Generate an app manifest json file
    new WebpackPwaManifest({
      // supports app manifest options: https://developer.mozilla.org/en-US/docs/Web/Manifest
      name: "WhenYouMove PWA",
      short_name: "WYM PWA",
      description: "The WhenYouMove App",
      theme_color: "#d9d9f9",
      background_color: "#FFF",
      crossorigin: "use-credentials", // can be null, use-credentials or anonymous
      icons: [
        {
          src: path.resolve("src/client/assets/images/when-you-move-icon.png"),
          sizes: [96, 128, 192, 256, 384, 512] // multiple sizes
        }
      ],
      ios: true, // use object instead of true to further customise (https://developer.apple.com/library/archive/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html)
      // Specify the sender id from firebase cloud messaging console
      ["gcm_sender_id" as string]: "448019731359"
    }),
    // adds the defer attribute to our bundles, this ensures that the server side rendered version is show first, before being hydrated on the client side
    // without the defer attribute, preloaded state sent from the server is overwritten
    new ScriptExtHtmlWebpackPlugin({ defaultAttribute: "defer" }),
    // creates a precache manifest (a list of webpack assets) and injects it into the service worker file
    new InjectManifest({
      swDest: "sw.js",
      swSrc: path.resolve("src", "client", "assets", "sw-template.js"),
      include: ["/app-shell", /\.js$/, /\.css$/],
      templatedUrls: {
        "/app-shell": new Date().toString()
      }
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: "static",
      openAnalyzer: false,
      reportFilename: "bundle-analysis.html"
    })
  ],
  optimization: {
    minimize: true,
    runtimeChunk: "single",
    splitChunks: {
      chunks: "all",
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName
            const packageName = module.context.match(
              /[\\/]node_modules[\\/](.*?)([\\/]|$)/
            )[1]

            // npm package names are URL-safe, but some servers don't like @ symbols
            return `npm.${packageName.replace("@", "")}`
          }
        }
      }
    }
  }
})

const server: webpack.Configuration = merge(baseConfig, {
  name: "server",
  target: "node",
  entry: path.resolve(__dirname, "src", "server", "index.tsx"),
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              configFile: "./babel.config.js"
            }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              hmr: process.env.NODE_ENV === "development"
            }
          },
          "css-loader"
        ]
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path]-[name].[hash].[ext]",
              outputPath: "images"
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader"]
      }
    ]
  },
  output: {
    filename: "server.js"
  },
  externals: [nodeExternals()]
})

export default [server, client]
