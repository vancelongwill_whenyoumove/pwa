# bash

set -e

# cd to directory of script
cd "${0%/*}"

COMPONENTS_DIR="../src/shared/components"

for d in $(find $COMPONENTS_DIR -maxdepth 1 -mindepth 1 -type d) 
do
  f=$(basename $d)
  fname="$d/$f.spec.tsx"
  if test -f $fname; then
    echo "Tests for $f already exists - skipping"
  else
    echo "Adding snapshot test for $f"
    sed -e "s/\${COMPONENT_NAME}/$f/g" snapshotTemplate.txt > $fname
  fi
done

