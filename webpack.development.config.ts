/* eslint-disable @typescript-eslint/camelcase */
import * as Dotenv from "dotenv-webpack"
import * as HtmlWebpackPlugin from "html-webpack-plugin"
import * as MiniCssExtractPlugin from "mini-css-extract-plugin"
import * as ScriptExtHtmlWebpackPlugin from "script-ext-html-webpack-plugin"
import * as webpack from "webpack"
import * as merge from "webpack-merge"
import * as nodeExternals from "webpack-node-externals"
import * as WebpackPwaManifest from "webpack-pwa-manifest"
import { InjectManifest } from "workbox-webpack-plugin"

export const baseConfig: webpack.Configuration = {
  mode: "development",
  devtool: "inline-source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    modules: ["src", "node_modules"]
  },
  plugins: [new Dotenv(), new webpack.HotModuleReplacementPlugin()]
}

const client: webpack.Configuration = merge(baseConfig, {
  name: "client",
  target: "web",
  entry: [
    "webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000",
    "./src/client/index.tsx"
  ],
  output: {
    filename: "bundle.[hash].js"
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              configFile: "./babel.config.client.js"
            }
          }
        ],
        exclude: [/node_modules/]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              hmr: process.env.NODE_ENV === "development"
            }
          },
          "css-loader"
        ]
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path]-[name].[hash].[ext]",
              outputPath: "images"
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader"]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new HtmlWebpackPlugin({
      title: "When You Move",
      template: "./src/shared/assets/index.html",
      // the output html file must not be called index.html, otherwise it will be resolved instead of the node app
      filename: "./public/dev-index.html"
    }),
    // Generate an app manifest json file
    new WebpackPwaManifest({
      // supports app manifest options: https://developer.mozilla.org/en-US/docs/Web/Manifest
      name: "WhenYouMove PWA",
      short_name: "WYM PWA",
      description: "The WhenYouMove App",
      theme_color: "#d9d9f9",
      background_color: "#FFF",
      crossorigin: "use-credentials", // can be null, use-credentials or anonymous
      icons: [
        {
          src: "src/client/assets/images/when-you-move-icon.png",
          sizes: [96, 128, 192, 256, 384, 512] // multiple sizes
        }
      ],
      ios: true, // use object instead of true to further customise (https://developer.apple.com/library/archive/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html)
      // Specify the sender id from firebase cloud messaging console
      ["gcm_sender_id" as string]: "448019731359"
    }),
    // adds the defer attribute to our bundles, this ensures that the server side rendered version is show first, before being hydrated on the client side
    // without the defer attribute, preloaded state sent from the server is overwritten
    new ScriptExtHtmlWebpackPlugin({ defaultAttribute: "defer" }),
    // creates a precache manifest (a list of webpack assets) and injects it into the service worker file
    new InjectManifest({
      swDest: "sw.js",
      swSrc: "src/client/assets/sw-template.js",
      include: ["/app-shell", /\.js$/, /\.css$/],
      templatedUrls: {
        "/app-shell": new Date().toString()
      }
    })
  ],
  stats: {
    errorDetails: true
  },
  optimization: {
    minimize: false,
    splitChunks: {
      chunks: "all"
    }
  }
})

const server: webpack.Configuration = merge(baseConfig, {
  name: "server",
  target: "node",
  node: {
    __dirname: false
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              configFile: "./babel.config.js"
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              hmr: process.env.NODE_ENV === "development"
            }
          },
          "css-loader"
        ]
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path]-[name].[hash].[ext]",
              outputPath: "images"
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader"]
      }
    ]
  },
  entry: ["./src/server/index.tsx"],
  output: {
    filename: "server.js"
  },
  externals: [nodeExternals()]
})

export default [server, client]
