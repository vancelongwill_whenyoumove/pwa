const presets = [
  "@babel/preset-react",
  [
    "@babel/env",
    {
      targets: {
        node: "current"
      }
    }
  ],
  "@babel/preset-typescript"
]

const plugins = [
  "transform-class-properties",
  "@babel/plugin-transform-runtime"
]

module.exports = api => {
  const isProduction = api.env("production")
  api.cache(true)
  // You can use isProduction to determine what presets and plugins to use.
  if (!isProduction) {
    plugins.unshift(["emotion", { sourceMap: true, autoLabel: true }])
  } else {
    plugins.unshift(["emotion", { hoist: true }])
  }

  return { presets, plugins }
}
