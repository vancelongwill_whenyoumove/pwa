// Mock api for authentication
import * as express from "express"

const server = express()
const port = 7000

server.use(express.json())

server.use((_, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  )
  next()
})

server.get("/ping", (_, res) => {
  res.status(200).send("pong")
})

server.post("/signin", (req, res) => {
  const token = "some-token"
  const user = req.body.user
  res.send({ user, token })
})

server.post("/register", (req, res) => {
  const token = "some-token"
  const user = req.body.user
  res.send({ user, token })
})

server.listen(port, () =>
  console.log(`WYM authentication listening on port ${port}!`)
)
