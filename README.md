### Install

- Clone this repo
- `npm i`
- `cp .env.sample .env` - Environment variables are injected from this file into the code via webpack

### Scripts

- You must also start the mock auth service with `npx ts-node authServer.ts` before running the project

- `npm run build` build the production bundles
- `npm run start` launch the production node app with SSR
- `npm run dev` for a hot reloading development server

  - HMR is enabled on the client and server side (up to `src/server/ssr.tsx`). If you make any changes to `src/server/index.tsx` you must still restart the server.
  - In order to keep the reloading fast, babel is used to transpile typescript in development mode. Types won't be checked on compilation. This should be done via your editor/ide or via the command `npm run types:watch`.

- `npm run format` format code with prettier
- `npm run types` check types (typescript compiler)
- `npm run types:watch` check types on detect changes (typescript compiler)
- `npm run lint` lint typescript & css
- `npm run lint:ts` lint typescript with tslint
- `npm run lint:css` lint css (in emotion template literals)
- `npm run lint:fix` autofix tslint
- `npm run test`run unit tests
- `npm run test:coverage` run unit tests & generate a coverage report

### Commits

- Commit messages should follow the format defined in `commitlint.config.js` [commitlint](https://github.com/conventional-changelog/commitlint)
- Before each commit, the staged files are formatted with prettier, and linted with tslint & stylelint
- Before each push, types are checked and tests are run

> NB: TypeScript compilation with '@babel/typescript' (no type checking)

### Protected routes

Routes requiring authentication should be placed in `src/shared/routes`, and given the addition key `requiresAuthentication: true`. If the client has not signed in, they will be directed to the Sign In view.

### SSR

- This app is isomorphic, meaning the same app is run on the server as the client

#### Data fetching

The application is rendered twice on the server side before sending to the client. This is to enable data prefetching managed by `redux-saga`. On the first pass, the component tree is rendered and actions are dispatched from components. The result of the async side effects of these actions are waited for, then the application is rendered once more with the new state. To ensure data prefetching occurs on the server side, the relevant action must be dispatched from the component's _constructor_ or the top-level of a function component, and must contain some kind of conditional logic to make sure they're not dispatched once more or the client side. Client side only actions should be dispatched from _componentDidMount_ or inside _useEffect_.

### Persistence

Persistence of redux state is implemented in a per-reducer basis with `redux-persist`. Due to the current limitations of `redux-persist`, prepopulating and persisting state is not available out of the box. To prevent bugs, persisted reducers are removed from the `window.__PRELOADED_STATE__` object sent from the server to the client. In order to achieve both preloading and persitence, the PERSIST action must be handled manually inside the relevant reducer, then the corresponding state key must be blacklisted (see the call to _removePersistedKeys_ in `src/server/ssr`).

#### Debugging

To see exactly what is rendered on the server side, disable javascript in chrome devtools (in settings).

### PWA features

- Client side persistence to localStorage
- SPA (routes are on the client side too)
- Service worker for caching assets (see `src/client/assets/sw-template.js` and `webpack.production.config.ts`)
  - The service worker should respond to all routes with a generic 'app shell'. In a client side only rendered app this would be `index.html` however as this project uses SSR it is instead the html returned by the `/app-shell` route (see `src/shared/routes.ts`)

### Static hosting

- Since our html & bundles are emitted to the `/public` directory, it's possible to bypass the server rendering completely and just serve the `/public` folder statically.
  e.g.
  ```
    npm run build-client
    serve -s public
  ```
